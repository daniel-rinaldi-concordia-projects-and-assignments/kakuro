\s{Architectural Design}
\label{ss:MVCDescription}
\ss{Description}
%This section must give a high-level description of the system in terms of its modules and their respective purpose and exact interface
The Kakuro app uses a Model View Controller (MVC) architecture. This architecture is made up of 3 separate application layers called Model, View, and Controller and are represented by the similarly named packages within the application source. The more specific type of MVC architecture used is the active model (also known as push model) wherein the Model layer is responsible for updating the view.
\\\par
The Model layer manages the data and state of the application. Data access and retrieval from the data store is also handled in this layer. Since we are using the active model variation of MVC, the model must update the view and it does so via the observer design pattern. The model acts as the subject (also known as observable) in the observer pattern and whenever it's state changes it notifies any View that has been registered with it. This layer contains the business logic of the application.
\\\par
The View refers to the application layer responsible for managing the user’s visualization of the application's state (presentation logic). The view components are registered as observers of the Model via the observer design pattern and whenever the Model updates its state the View will get notified and it will update itself accordingly. The Kakuro app uses a GUI framework for the view and as such, in order to capture user input, a Listener (which is specific to the GUI framework) is needed. The Listener object is attached to the view and its only purpose is to relay the user's input to the controller.
\\\par
The Controller refers to the application layer that handles input from the user, that may require the model to be modified. The controller contains the application's control logic and as such, it decides what action to perform based on the user's input. The controller may also update the view as well.
\newpage

\ss{Architectural Diagram}
%A UML class diagram or package diagram depicting the high-level structure of the system, accompanied by a one-paragraph text describing the rationale of this design.  It is mandatory that the system be divided into at least two subsystems, and that the purpose of each of these subsystems be exposed here.
\begin{figure}[H]
    \centering
    \includegraphics[scale=1.0]{MVC_package_diagram.png}
    \caption{MVC High Level Package Diagram}
    \label{fig:MVCHLD}
\end{figure}

The MVC architecture provides a clear separation of the presentation logic, the control logic, and the business logic. This separation of concerns between the layers of the MVC architecture improves maintainability, testability, and scalability of the app. By using this approach our team can focus on different parts (layers) of the app without really affecting other parts. Also, this approach is good for portability as well. If our team decides to use a more modern GUI framework for the app then the view can be simply swapped out for a new view that utilizes the new GUI without needing to refactor the controller and the model. Finally, the MVC architecture allows for multiple developers to work on a feature simultaneously due to the nature of MVC which would separate it into its 3 separate layers (model, view, and controller).
\newpage

\ss{Subsystem Interface Specifications}
%Specification of the software interfaces between the subsystems, i.e.  specific messages (or function calls) that are exchanged by the subsystems.  These are also often called “Module Interface Specifications”.  Description of the parameters to be passed into these function calls in order to have a service fulfilled, including valid and invalid ranges of values. Each subsystem interface must be presented in a separate subsection
%refer to section 3.2 of montrealopoly

\sss{Module Initialization}
The many modules used within the application need to be initialized before use. This is done at the same time that the Kakuro game is instantiated. The game is instantiated by creating an instance of the KakuroApp class. The constructor will create an instance of each module and initialize them. Then it starts the main GUI loop that keeps displaying the main window of the app (AppView class) until the GUI is disposed by user request. 

\sss{The View Interface}
The view interface is accessed by the model and the controller layer. All view modules stem from some GUI component (These are called "controls" in the GUI framework we are using). As such, every view has a root control that may or may not contain other controls. A control may contain other controls is called a "composite". Each view module inherits from an abstract class called "View" and implements the following methods:
\begin{itemize}[noitemsep]
\item \textbf{getRootComposite()}: This method simply returns the root composite associated with this view.
\item \textbf{setupGUI()}: This method is mainly for setting up and initializing the GUI components of this view. This method can be overloaded if needed.
\end{itemize}
The View class inherits from the IObserver interface and as such, every view class implements the following methods:
\begin{itemize}[noitemsep]
\item \textbf{onNotify()}: This method is part of the observer pattern and is the way in which the model notifies the view of any changes.
\end{itemize}
\noindent The Kakuro Application currently contains the following view modules:

\paragraph{AppView}
The AppView class is a view that contains the presentation logic required for displaying the main window of the Kakuro app. At the moment, there is no model for the AppView. The AppView is responsible for displaying a menu bar that allows the user to perform various actions like quitting the game or asking for help. It also has a main area where a partial view can be drawn. The AppView class contains the following service methods:
\begin{itemize}[noitemsep]
\item \textbf{setStackView()}: This method sets the partial view to be displayed in the main area of the window.
\item \textbf{getStackComposite()}: This method gets the composite that represents the area that contains the stacked partial views.
\item \textbf{open()}: opens and displays the main window.
\end{itemize}
\newpage

\paragraph{PuzzleView}
The PuzzleView class is a partial view that contains the presentation logic required for displaying the puzzle. When this module is initialized, it uses the PuzzleDAO class to read from the puzzle database and create a puzzle object. It then registers itself as an observer on the the puzzle model. When the state of the puzzle model changes, it will notify any observer of the changes through the use of the \textbf{onNotify()} method which it inherits from the IObserver class. 

\paragraph{WelcomeView}
The WelcomeView class is a partial view that contains the presentation logic required for showing the landing page of the app. This view only contains a navigation menu with options to play or quit the game. The former of the two simply switches to the PuzzleView via the \textbf{setStackView()} method of the AppView class. Due to the simplicity of this view, there is no exposed service methods.

\sss{The Model Interface}
The model interface is accessed by the controller layer. All model modules inherit from ISubject interface (part of the observer pattern). As such, if a model module is to observed, then it implements the following methods from ISubject:
\begin{itemize}[noitemsep]
\item \textbf{registerObservers()}: This method is used to register a view module with this model module.
\item \textbf{unregisterObserver()}: This method is used to unregister a given view module with this model module.
\item \textbf{getObservers()}: This method gets the list of IObservers that are registered with this module.
\end{itemize}

\noindent The model layer contains the following modules:

\paragraph{Puzzle}
The Puzzle class represents a kakuro puzzle and contains the business logic responsible for any operations on it. A Puzzle owns and is composed of a group of Cells (see sections \hr{sec:section2_3_3_2}{2.3.3.2} - \hr{sec:section2_3_3_4}{2.3.3.4}) that make up the puzzle. The puzzle class exposes the following methods: 
\begin{itemize}[noitemsep]
\item \textbf{getCellMatrix()}: This method returns the 2D array of Cells that make up the puzzle.
\item \textbf{editCellValue()}: changes the value of the cell at a given position (x, y) to the given character parameter. The character parameter must be a digit and must be between 1 and 9 inclusive.
\item \textbf{deleteCellValue()}: Simply changes the value of the cell at a given position (x, y) to null.
\item \textbf{checkSolution()}: checks the solution of this Puzzle to see if it is a correct soution with respect to the rules of kakuro and returns the result of this check.
\item \textbf{resetPuzzle()}: Simply changes the value of all ValueCells of this Puzzle to null.
\end{itemize}

\paragraph{Cell} \label{sec:section2_3_3_2}
The Cell class represents an empty kakuro puzzle cell and contains the business logic responsible for any operations on it. Currently, there is no business logic associated with empty cells. All other types of cells inherit from this class.

\paragraph{SumCell}
The SumCell class represents a kakuro puzzle cell that contains sums for adjacent cells as per the rules of kakuro. It contains the business logic responsible for any operations on it. The SumCell class exposes the following methods:
\begin{itemize}[noitemsep]
\item \textbf{getDownSum() , setDownSum()}: These methods simply get and set the downwards sum value of the SumCell.
\item \textbf{getRightSum() , setRightSum()}: These methods simply get and set the rightwards sum value of the SumCell.
\end{itemize}

\paragraph{ValueCell} \label{sec:section2_3_3_4}
The ValueCell class represents a kakuro puzzle cell that holds a digit value that the user can modify as per the rules of kakuro. It contains the business logic responsible for any operations on it. The ValueCell class exposes the following methods:
\begin{itemize}[noitemsep]
\item \textbf{getValue()}: Simply gets the value associated with this ValueCell.
\item \textbf{setValue()}: Sets the value associated with this ValueCell and notifies any observers of the change. This triggers the interaction between the model and view layers via the observer pattern.
\item \textbf{getX() , setX() , getY() , setY()}: Every ValueCell also stores its position in the puzzle's matrix of cells. It does so using these accessors and allows access to this data using these mutators. Finally, since a Puzzle is composed of many cells, when a puzzle is instantiated it also sets the position of its ValueCells by calling these methods.
\end{itemize}

\paragraph{SaveObject}
The SaveObject class represents save data for a kakuro puzzle. It contains the business logic responsible for any operations on it. The SaveObject class exposes the following methods:
\begin{itemize}[noitemsep]
\item \textbf{getId()}: Simply returns the id of the puzzle this SaveObject refers to.
\item \textbf{getValueCellData(), setValueCellData()}: Gets and sets the value cell data from this SaveObject respectively. These methods return and accept a array of Strings respectively that represent each value cell in a puzzle sequentially from top to bottom and left to right. The setValueCellData() method is overloaded to also accept a Puzzle as a parameter.
\item \textbf{setValue()}: Sets the value associated with this ValueCell and notifies any observers of the change. This triggers the interaction between the model and view layers via the observer pattern.
\item \textbf{getX() , setX() , getY() , setY()}: Every ValueCell also stores its position in the puzzle's matrix of cells. It does so using these accessors and allows access to this data using these mutators. Finally, since a Puzzle is composed of many cells, when a puzzle is instantiated it also sets the position of its ValueCells by calling these methods.
\end{itemize}

\noindent The model layer also contains a sub layer, the DAO (Data Access Object) layer. This layer is accessed by the controller layer as well. The DAO layer contains modules responsible for doing CRUD (Create, Read, Update, Delete) operations on different types of data stores. Every DAO class implements a generic IDAO interface which has the following methods which represent CRUD operations on the data stores:
\begin{itemize}[noitemsep]
\item \textbf{get()}: Gets a resource of the specified type from the database with the given id.
\item \textbf{getAll()}: Gets a list of all resources of the specified type from the database.
\item \textbf{save()}: Creates a resource of the specified type in the database.
\item \textbf{update()}: Updates a resource of the specified type in the database.
\item \textbf{delete()}: Deletes a resource of the specified type from the database.
\end{itemize}

\sss{The Controller Interface}
The controller interface is accessed by the controller layer. All controller modules inherit from a base Controller class. The controller modules expose methods that are to be called by the view layer through the use of our GUI framework's various Listener classes. A Listener object is instantiated at the time of creating the view and is attached to a GUI control that listens for input from the user (ex: buttons, textboxes, etc). These listeners simply relay the user input to a controller module and it is only within this controller layer that the user's input is handled and it is decided what to do based on the input received. The controller layer contains the following modules: 

\paragraph{AppController}
The AppController is responsible for performing any actions based on user input coming from a GUI component of the AppView. Currently, there is no functionality for this controller because the basic functionality of the AppView (resizing, minimizing, closing the app) is handled internally by the GUI framework already. However, this class remains here in case of additional functionality specific to this view that may be added in the future.

\paragraph{PuzzleController}
The PuzzleController is responsible for performing any actions based on user input coming from a GUI component of the PuzzleView. The PuzzleController currently exposes the following service methods for handling user input: 
\begin{itemize}[noitemsep]
\item \textbf{editCellValue()}: Called when the user edits a value cell in the PuzzleView. This method calls the editCellValue() method of the current puzzle.
\item \textbf{deleteCellValue()}: Called when the user clears a value cell in the PuzzleView. This method calls the deleteCellValue() method of the current puzzle.
\item \textbf{checkSolution()}: Called when the user clicks the "check solution" button in the PuzzleView. This method calls the checkSolution() method of the current puzzle. 
\item \textbf{saveProgress()}: Called when the user clicks the "save my progress" button in the PuzzleView. This creates an instance of the SaveFileDAO class and calls its method which writes to the database. 
\item \textbf{loadSavedGame()}: Called when the user clicks the "load" button in the PuzzleView. This creates an instance of the SaveFileDAO class and calls its method which loads the save data of that puzzle into the current puzzle.
\item \textbf{resetBoard()}: Called when the user clicks the "reset" button in the PuzzleView. This method calls the resetPuzzle() method of the current puzzle.
\end{itemize}
