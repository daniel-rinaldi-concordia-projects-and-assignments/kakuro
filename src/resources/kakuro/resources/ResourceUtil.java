package kakuro.resources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.wb.swt.SWTResourceManager;

public class ResourceUtil extends SWTResourceManager
{	
	// Images
	public static final String IMAGES_DIR = "images/";
	public static final String IMG_FAVICON = IMAGES_DIR + "favicon.png";
	public static final String IMG_KAKURO_TITLE = IMAGES_DIR + "kakuro_title.png";
	public static final String IMG_WELCOME_BG = IMAGES_DIR + "welcome_bg.jpg";
	
	// Assets
    public static final String ASSETS_DIR = "assets/";
    public static final String ASSETS_FONTS_DIR = ASSETS_DIR + "fonts/";
    public static final String ASSET_FONT_ROBOTO_LICENSE = ASSETS_FONTS_DIR + "roboto/LICENSE.txt";
    public static final String ASSET_FONT_ROBOTO = ASSETS_FONTS_DIR + "roboto/Roboto-Regular.ttf";
    public static final String ASSET_FONT_ROBOTO_LIGHT = ASSETS_FONTS_DIR + "roboto/Roboto-Light.ttf";
    
    // Data files
    public static final String DATA_DIR = "data/";
    public static final String DATA_PUZZLES_DIR = DATA_DIR + "puzzles/";
    public static final String DATA_SAVE_DIR = DATA_DIR + "saved/";
    /**
	 * Maps fonts to their resized versions.
	 */
	private static Map<Font, Font> fontToResizedFontMap = new HashMap<Font, Font>();
    
    /**
     * Get a resource on the classpath as an input stream <br>
     * IMPORTANT: returned InputStream must be closed manually via {@link InputStream#close()} when it's no longer needed.
     * 
     * @param clazz the class that is used for the relative path's root 
     * @param path the relative path to the requested resource
     * @return {@link InputStream} of a resource located at the specified path relative to clazz
     * @throws FileNotFoundException if requested resource is not found
     */
    public static InputStream getResourceAsStream(Class<?> clazz, String path) throws FileNotFoundException
    {
    	if(clazz.getResource(path) == null)
    	{
    		throw new FileNotFoundException("Resource not found: " + path);
    	}
    	
    	InputStream istream = null;
    	try
    	{
    		istream = clazz.getResourceAsStream(path);
    	}
    	catch(NullPointerException e)
    	{
    		System.err.println(ResourceUtil.class.getCanonicalName() + " -- Error: resource path string was null.");
    	}
    	
    	return istream;
    }
    /**
     * delegates work to {@link #getResourceAsStream(Class, String)} using ResourceUtil.class as the Class.
     */
    public static InputStream getResourceAsStream(String path) throws FileNotFoundException
    {
    	return getResourceAsStream(ResourceUtil.class, path);
    }
    
    /**
     * Copies a resource on the classpath to the filesystem
     * @param path the path to the resource on the classpath relative to {@link ResourceUtil}
     * @param outputPath the path on the filesystem where the resource should be copied to
     * @throws FileNotFoundException if either the resource is not found on the classpath or, 
     * for the filesystem target, if the file exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for any other reason
     * @throws IOException if an I/O error occurs
     */
    public static void copyResourceToFileSystem(String path, String outputPath) throws FileNotFoundException, IOException
    {
    	InputStream istream = getResourceAsStream(path);
		byte[] buffer = new byte[istream.available()];
    	istream.read(buffer);
    	
	    File targetFile = new File(outputPath);
	    targetFile.getParentFile().mkdirs();
	    targetFile.createNewFile();
	    OutputStream outStream = new FileOutputStream(targetFile);
	    outStream.write(buffer);
	    outStream.close();
	    istream.close();
    }
    
    /**
     * Creates and caches a new Font based off the supplied base font and height
     * @param baseFont the font to resize
     * @param height the height of the new font
     * @return a newly created Font of the given height if one does not already exist in the cache
     */
    public static Font getResizedFont(Font baseFont, int height)
    {
    	Font font = fontToResizedFontMap.get(baseFont);
		if (font == null) 
		{
			FontData fontDatas[] = baseFont.getFontData();
			FontData data = fontDatas[0];
			data.setHeight(height);
			font = new Font(baseFont.getDevice(), data);
			fontToResizedFontMap.put(baseFont, font);
		}
		return font;
    }
    
    /**
     * Dispose of cached objects and their underlying OS resources. 
     * This should only be called when the cached objects are no longer needed (e.g. on application shutdown).
     */
    public static void dispose() 
    {
    	SWTResourceManager.dispose();
    	
    	// dispose Fonts
    	for (Font font : fontToResizedFontMap.values())
    		font.dispose();
    }
}
