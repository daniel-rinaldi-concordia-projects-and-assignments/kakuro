package kakuro;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	InitializeBoardTest.class,
	EditingCellTest.class,
	SolutionCheckerTest.class,
	ResetPuzzleTest.class,
	LoadSavedPuzzleTest.class
})

/**
 * @author Marc Vicuna
 * Modified by: 
 *
 */
public class MainTest {   
}
