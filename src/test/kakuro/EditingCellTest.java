package kakuro;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.controller.PuzzleController;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.ValueCell;
import kakuro.model.data.PuzzleDAO;

/**
 * @author Marc Vicuna
 * Modified by: 
 * 
 *
 */
public class EditingCellTest 
{
	private static Logger logger = LoggerFactory.getLogger(EditingCellTest.class);
	
	/**
	 * Announcing the tests performed.
	 */
	@BeforeClass 
	public static void message() {
		logger.debug("Running tests for cell editing.");
	}
	/**
	 * Testing whether the function edits the cells when given valid input.
	 */
	@Test
	public void EditingCorrectly() {
		editing('9', true);
	}
	/**
	 * Testing whether the function edits the cells when given lowercase letters.
	 */
	@Test
	public void EditingIncorrectly1() {
		editing('n', false);
	}
	/**
	 * Testing whether the function edits the cells when given uppercase letters.
	 */
	@Test
	public void EditingIncorrectly2() {
		editing('K', false);
	}
	/**
	 * Testing whether the function edits the cells when given 0.
	 */
	@Test
	public void EditingIncorrectly3() {
		editing('0', false);
	}
	/**
	 * Testing whether the function edits the cells when given a symbol outside of alphanumeric characters.
	 */
	@Test
	public void EditingIncorrectly4() {
		editing('_', false);
	}
	/**
	 * Method to test any input character, to determine which characters are accepted.
	 * @param input the character given to the cell.
	 * @param valid whether the test is supposed to be true or not
	 */
	private void editing(char input, boolean valid) {
		KakuroApp app = new KakuroApp();
		PuzzleDAO test = new PuzzleDAO();
		Puzzle puzzle = test.get(0);
		Cell[][] matrix = puzzle.getCellMatrix();
		PuzzleController p = new PuzzleController(app,puzzle);
		p.editCellValue(5,4,'6');
		p.editCellValue(5,4,input);
		if (valid)
		{
			assertEquals(Character.getNumericValue(input),((ValueCell)matrix[5][4]).getValue().intValue());
			if(Character.getNumericValue(input)==((ValueCell)matrix[5][4]).getValue().intValue())
				logger.debug("Successful test " + input);
			else
				logger.debug("Failure of test " + input);
		}
		else
		{
			assertEquals(6,((ValueCell)matrix[5][4]).getValue().intValue());
			if(6==((ValueCell)matrix[5][4]).getValue().intValue())
				logger.debug("Successful test " + input);
			else
				logger.debug("Failure of test " + input);
		}
	}
	
}
