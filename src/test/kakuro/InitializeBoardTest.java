package kakuro;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.SumCell;
import kakuro.model.data.PuzzleDAO;

/**
 * @author Marc Vicuna
 * Modified by: 
 *
 */
public class InitializeBoardTest 
{
	private static Logger logger = LoggerFactory.getLogger(InitializeBoardTest.class);
	
	/**
	 * Announcing the tests performed.
	 */
	@BeforeClass 
	public static void message() {
		logger.debug("Running tests of board initialisation.");
	}
	/**
	 * Testing 7x7 board.
	 */
	@Test
	public void initializeBoard1() {
		PuzzleDAO test = new PuzzleDAO();
		Puzzle puzzle1 = test.get(1);
		Cell[][] matrix = puzzle1.getCellMatrix();
		boolean matches = true;
		matches &= ((SumCell)matrix[0][1]).getDownSum()==19;
		matches &= ((SumCell)matrix[0][2]).getDownSum()==9;
		matches &= ((SumCell)matrix[0][4]).getDownSum()==24;
		matches &= ((SumCell)matrix[0][5]).getDownSum()==12;
		matches &= ((SumCell)matrix[0][6]).getDownSum()==16;
		matches &= ((SumCell)matrix[1][0]).getRightSum()==4;
		matches &= ((SumCell)matrix[1][3]).getRightSum()==23;
		matches &= ((SumCell)matrix[2][0]).getRightSum()==10;
		matches &= ((SumCell)matrix[2][3]).getDownSum()==18;
		matches &= ((SumCell)matrix[2][3]).getRightSum()==13;
		matches &= ((SumCell)matrix[3][0]).getRightSum()==24;
		matches &= ((SumCell)matrix[3][5]).getDownSum()==21;
		matches &= ((SumCell)matrix[3][6]).getDownSum()==11;
		matches &= ((SumCell)matrix[4][1]).getDownSum()==9;
		matches &= ((SumCell)matrix[4][2]).getDownSum()==10;
		matches &= ((SumCell)matrix[4][2]).getRightSum()==27;
		matches &= ((SumCell)matrix[5][0]).getRightSum()==7;
		matches &= ((SumCell)matrix[5][4]).getRightSum()==13;
		matches &= ((SumCell)matrix[6][0]).getRightSum()==23;
		matches &= ((SumCell)matrix[6][4]).getRightSum()==5;
		if(matches)
			logger.debug("Successful test 7x7.");
		else
			logger.debug("Failure of test 7x7.");
		assertTrue(matches);
	}
	/**
	 * Testing 10x10 board.
	 */
	@Test
	public void initializeBoard0() {
		PuzzleDAO test = new PuzzleDAO();
		Puzzle puzzle = test.get(0);
		Cell[][] matrix = puzzle.getCellMatrix();
		boolean matches = true;
		matches &= ((SumCell)matrix[0][3]).getDownSum()==4;
		matches &= ((SumCell)matrix[0][4]).getDownSum()==16;
		matches &= ((SumCell)matrix[0][6]).getDownSum()==24;
		matches &= ((SumCell)matrix[0][7]).getDownSum()==3;
		matches &= ((SumCell)matrix[1][2]).getRightSum()==8;
		matches &= ((SumCell)matrix[1][5]).getDownSum()==7;
		matches &= ((SumCell)matrix[1][5]).getRightSum()==11;
		matches &= ((SumCell)matrix[1][8]).getDownSum()==30;
		matches &= ((SumCell)matrix[2][2]).getRightSum()==30;
		matches &= ((SumCell)matrix[2][9]).getDownSum()==3;
		matches &= ((SumCell)matrix[3][3]).getDownSum()==24;
		matches &= ((SumCell)matrix[3][4]).getDownSum()==16;
		matches &= ((SumCell)matrix[3][4]).getRightSum()==11;
		matches &= ((SumCell)matrix[3][7]).getDownSum()==6;
		matches &= ((SumCell)matrix[3][7]).getRightSum()==11;
		matches &= ((SumCell)matrix[4][2]).getDownSum()==10;
		matches &= ((SumCell)matrix[4][2]).getRightSum()==17;
		matches &= ((SumCell)matrix[4][6]).getDownSum()==17;
		matches &= ((SumCell)matrix[4][6]).getRightSum()==11;
		matches &= ((SumCell)matrix[5][1]).getDownSum()==3;
		matches &= ((SumCell)matrix[5][1]).getRightSum()==18;
		matches &= ((SumCell)matrix[5][5]).getDownSum()==24;
		matches &= ((SumCell)matrix[5][5]).getRightSum()==15;
		matches &= ((SumCell)matrix[6][0]).getRightSum()==11;
		matches &= ((SumCell)matrix[6][4]).getDownSum()==7;
		matches &= ((SumCell)matrix[6][4]).getRightSum()==20;
		matches &= ((SumCell)matrix[7][0]).getRightSum()==6;
		matches &= ((SumCell)matrix[7][3]).getDownSum()==16;
		matches &= ((SumCell)matrix[7][3]).getRightSum()==8;
		matches &= ((SumCell)matrix[7][6]).getDownSum()==17;
		matches &= ((SumCell)matrix[7][7]).getDownSum()==3;
		matches &= ((SumCell)matrix[8][1]).getRightSum()==31;
		matches &= ((SumCell)matrix[9][2]).getRightSum()==11;
		matches &= ((SumCell)matrix[9][5]).getRightSum()==11;
		if(matches)
			logger.debug("Successful test 10x10.");
		else
			logger.debug("Failure of test 10x10.");
		assertTrue(matches);
	}

}
