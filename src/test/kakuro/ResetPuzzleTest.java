package kakuro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.KakuroApp;
import kakuro.controller.PuzzleController;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.ValueCell;
import kakuro.model.data.PuzzleDAO;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

public class ResetPuzzleTest {

	private static Logger logger = LoggerFactory.getLogger(ResetPuzzleTest.class);
	
	/**
	 * Announcing the tests performed.
	 */
	@BeforeClass 
	public static void message() {
		logger.debug("Running tests for the resetting of the puzzle.");
	}
	
	/**
	 * Testing whether the function clears the board.
	 */
	@Test
	public void clearedBoard() {
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		puzzle.resetPuzzle();
		logger.debug("Testing if all cells are cleared");
		boolean itWorked = true;
		for (Cell[] row : matrix) 
		{
			for (Cell cell : row) 
			{
				if (cell instanceof ValueCell) 
				{ 
					//if valueCell
					Integer value = ((ValueCell) cell).getValue();
					if (value != null) { //checks if any value cells have value entered
						logger.debug("Cell not cleared " + ((ValueCell) cell).getValue());
						itWorked = false;
					}
					if (itWorked == true) {
						logger.debug("Board was cleared successfully!");
					}
				}
			}
		}
		assertTrue(itWorked);
		logger.debug("Test comepleted.");
	}
	
	/**
	 * Testing whether entering a digit works after clearing the board.
	 */
	@Test
	public void canUseClearedBoard() {
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		puzzle.resetPuzzle();
		logger.debug("Testing if a cell can be modified after being cleared.");
		boolean canWriteAfterCleared = false;
		((ValueCell)matrix[1][5]).setValue(2);
		if (((ValueCell)matrix[1][5]).getValue() == 2) {
			logger.debug("Board was updated after a clear successfully!");
			canWriteAfterCleared = true;
		}
		else {
			logger.debug("Board was not updated after a clear...");
		}
		assertTrue(canWriteAfterCleared);
	}

	
	private void solution(Cell[][] matrix) {
		((ValueCell)matrix[1][5]).setValue(8);
		((ValueCell)matrix[1][6]).setValue(9);
		((ValueCell)matrix[2][4]).setValue(2);
		((ValueCell)matrix[3][2]).setValue(5);
		((ValueCell)matrix[3][4]).setValue(7);
		((ValueCell)matrix[4][3]).setValue(4);
		((ValueCell)matrix[4][6]).setValue(6);
		((ValueCell)matrix[5][5]).setValue(9);
		((ValueCell)matrix[5][6]).setValue(4);
		((ValueCell)matrix[6][2]).setValue(6);
		((ValueCell)matrix[6][5]).setValue(4);
	}
}
