package kakuro;

import java.io.InputStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.SaveObject;
import kakuro.model.ValueCell;
import kakuro.model.data.PuzzleDAO;
import kakuro.resources.ResourceUtil;

/**
 * @author Jason
 * Modified by: 
 *
 */
public class LoadSavedPuzzleTest
{
	private static Logger logger= LoggerFactory.getLogger(LoadSavedPuzzleTest.class);
	
	/**
	 * announcing the test
	 */
	@BeforeClass
	public static void message()
	{
		logger.debug("Testing the data loaded from saved puzzle");
	}
	/**
	 * 
	 *testing the load feature. Loads a set puzzle from {@link}load-test.data and check that the correct 
	 *value has been entered. 
	 */
	@Test
	public void loadTest()
	{
		SaveObject save = new SaveObject(0);
		Puzzle puzzle = new PuzzleDAO().get(0);
		Cell[][] matrix = puzzle.getCellMatrix();
		boolean matches = true; 
		InputStream testIstream = null;
		try
		{
			//gets the test puzzle 
			testIstream = ResourceUtil.getResourceAsStream(ResourceUtil.class, ResourceUtil.DATA_SAVE_DIR + "load-test.dat");
			BufferedReader reader = new BufferedReader(new InputStreamReader(testIstream));
				String line = reader.readLine();
				save.setValueCellData(line.split(","));
				reader.close();
				puzzle.load(save);
				matrix=puzzle.getCellMatrix();
				//checks if the value loaded into the test puzzle is what correct. in this case 9
				//logger.debug(( Integer.toString(((ValueCell) matrix[1][4]).getValue() )));
				matches &= (((ValueCell)puzzle.getCellMatrix()[1][4]).getValue() == 9);
				if(matches) 
				{
					logger.debug("values correclty loaded");
					assertTrue(matches);
				}
				else
				{
					logger.debug("values incorreclty loaded");
					assertFalse(matches);
				}
		}
		catch(FileNotFoundException e) 
		{
			System.err.println(LoadSavedPuzzleTest.class.getCanonicalName() +"--- Error: " + e.getMessage());
		}
		catch (IOException e) 
		{
			System.err.println(LoadSavedPuzzleTest.class.getCanonicalName() + " -- Error: " + e.getMessage());
		}
		}
	@Test
	/**
	 * Test loads saved puzzle and checks to see if the loaded cell can be correctly modified after a load.
	 */
	public void modifyCellAfterLoad()
	{
			SaveObject save = new SaveObject(0);
		Puzzle puzzle = new PuzzleDAO().get(0);
		Cell[][] matrix = puzzle.getCellMatrix();
		boolean matches = false; 
		InputStream testIstream = null;
		try
		{
			//gets the test puzzle 
			testIstream = ResourceUtil.getResourceAsStream(ResourceUtil.class, ResourceUtil.DATA_SAVE_DIR + "load-test.dat");
			BufferedReader reader = new BufferedReader(new InputStreamReader(testIstream));
				String line = reader.readLine();
				save.setValueCellData(line.split(","));
				reader.close();
				puzzle.load(save);
				matrix=puzzle.getCellMatrix();
				//matches &= (((ValueCell)puzzle.getCellMatrix()[1][4]).getValue() == 9);
				//checks to see if loaded value can be over written
				((ValueCell)matrix[1][4]).setValue(3);
				if(((ValueCell)matrix[1][4]).getValue() == 3) 
				{
					matches = true;
					logger.debug("Value has been correctly updated");
					assertTrue(matches);
				}
				else
				{
					logger.debug("values has not been updated");
					assertFalse(matches);
				}
		}
		catch(FileNotFoundException e) 
		{
			System.err.println(LoadSavedPuzzleTest.class.getCanonicalName() +"--- Error: " + e.getMessage());
		}
		catch (IOException e) 
		{
			System.err.println(LoadSavedPuzzleTest.class.getCanonicalName() + " -- Error: " + e.getMessage());
		}
		}

}
	/*need to initialize a puzzle and then need to populate it with values from a file. The test 
	 * passed if the generated puzzle has the values from the saved file in the correct place.*/
