package kakuro;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.controller.PuzzleController;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.ValueCell;
import kakuro.model.data.PuzzleDAO;

/**
 * @author Marc Vicuna
 * Modified by: 
 *
 */
public class SolutionCheckerTest 
{
	private static Logger logger = LoggerFactory.getLogger(SolutionCheckerTest.class);
	
	/**
	 * Announcing the tests performed.
	 */
	@BeforeClass 
	public static void message() {
		logger.debug("Running tests for the solution checking.");
	}
	/**
	 * Testing whether the function spots good solutions.
	 */
	@Test
	public void SolutionCorrect() {
		KakuroApp app = new KakuroApp();
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		boolean result = new PuzzleController(app,puzzle).checkSolution();
		assertTrue(result);
		if(result)
			logger.debug("Successful test Correct.");
		else
			logger.debug("Failure of test Correct.");
	}
	
	/**
	 * Testing whether the function spots incorrect sums.
	 */
	@Test
	public void SolutionIncorrect1() {
		KakuroApp app = new KakuroApp();
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		((ValueCell)matrix[1][1]).setValue(4); //the sum does not correspond to the SumCell values
		boolean result = new PuzzleController(app,puzzle).checkSolution();
		assertFalse(result);
		if(!result)
			logger.debug("Successful test Bad Sums.");
		else
			logger.debug("Failure of test Bad Sums.");
	}
	
	/**
	 * Testing whether the function spots empty cells.
	 */
	@Test
	public void SolutionIncorrect2() {
		KakuroApp app = new KakuroApp();
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		((ValueCell)matrix[1][1]).setValue(null); //some ValueCell does not have any value
		boolean result = new PuzzleController(app,puzzle).checkSolution();
		assertFalse(result);
		if(!result)
			logger.debug("Successful test Empty Cell.");
		else
			logger.debug("Failure of test Empty Cell.");
	}
	/**
	 * Testing whether the function spots integer repetition.
	 */
	@Test
	public void SolutionIncorrect3() {
		KakuroApp app = new KakuroApp();
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		((ValueCell)matrix[5][1]).setValue(3); //changed, was 1
		((ValueCell)matrix[5][2]).setValue(2); //changed, was 4
		((ValueCell)matrix[6][1]).setValue(6); // changed, was 8
		((ValueCell)matrix[6][2]).setValue(8); // changed, was 6
		boolean result = new PuzzleController(app,puzzle).checkSolution();
		assertFalse(result);
		if(!result)
			logger.debug("Successful test Number Repetition.");
		else
			logger.debug("Failure of test Number Repetition.");
	}
	/**
	 * Inputs the solution to the puzzle.
	 * @param matrix matrix of all cells
	 */
	private void solution(Cell[][] matrix) {
		((ValueCell)matrix[1][1]).setValue(3);
		((ValueCell)matrix[1][2]).setValue(1);
		((ValueCell)matrix[1][4]).setValue(6);
		((ValueCell)matrix[1][5]).setValue(8);
		((ValueCell)matrix[1][6]).setValue(9);
		((ValueCell)matrix[2][1]).setValue(7);
		((ValueCell)matrix[2][2]).setValue(3);
		((ValueCell)matrix[2][4]).setValue(2);
		((ValueCell)matrix[2][5]).setValue(4);
		((ValueCell)matrix[2][6]).setValue(7);
		((ValueCell)matrix[3][1]).setValue(9);
		((ValueCell)matrix[3][2]).setValue(5);
		((ValueCell)matrix[3][3]).setValue(3);
		((ValueCell)matrix[3][4]).setValue(7);
		((ValueCell)matrix[4][3]).setValue(4);
		((ValueCell)matrix[4][4]).setValue(9);
		((ValueCell)matrix[4][5]).setValue(8);
		((ValueCell)matrix[4][6]).setValue(6);
		((ValueCell)matrix[5][1]).setValue(1);
		((ValueCell)matrix[5][2]).setValue(4);
		((ValueCell)matrix[5][3]).setValue(2);
		((ValueCell)matrix[5][5]).setValue(9);
		((ValueCell)matrix[5][6]).setValue(4);
		((ValueCell)matrix[6][1]).setValue(8);
		((ValueCell)matrix[6][2]).setValue(6);
		((ValueCell)matrix[6][3]).setValue(9);
		((ValueCell)matrix[6][5]).setValue(4);
		((ValueCell)matrix[6][6]).setValue(1);
	}
}
