package kakuro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.KakuroApp;
import kakuro.controller.PuzzleController;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.SaveObject;
import kakuro.model.ValueCell;
import kakuro.model.data.PuzzleDAO;
import kakuro.model.data.SaveFileDAO;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.BeforeClass;
import org.junit.Test;

public class SaveProgressTest {

	private static Logger logger = LoggerFactory.getLogger(ResetPuzzleTest.class);

	/**
	 * Announcing the tests performed.
	 */
	@BeforeClass 
	public static void message() {
		logger.debug("Running tests for the resetting of the puzzle.");
	}
	
	/**
	 * Testing whether the function opens a file successfully.
	 */
	@Test
	public void fileWasCreated() {
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		SaveObject save = new SaveObject(0);
		save.setValueCellData(puzzle);
		logger.debug("Testing successfully opened file.");
		boolean wasSuccessful = false;
		if(new SaveFileDAO().save(save)) {
			logger.debug("The file was successfully opened!!");
			wasSuccessful = true;
		}
		else {
			logger.debug("The file was not opened.");
		}
		assertTrue(wasSuccessful);
	}
	
	/**
	 * Testing whether the function saved the proper values, assuming a successful save.
	 */
	@Test
	public void savedOutputMatchesPuzzle() {
		//this first part assumes the previous test passed with success - file saved
		Puzzle puzzle = new PuzzleDAO().get(1);
		Cell[][] matrix = puzzle.getCellMatrix();
		solution(matrix);
		SaveObject save = new SaveObject(0);
		save.setValueCellData(puzzle);
		logger.debug("Assuming file saved successfully.");
		
		//this second part will open the file and read the entries, matching them to the sample solution puzzle to validate it
		Puzzle puzzle1 = new PuzzleDAO().get(1);
		Cell[][] matrix1 = puzzle1.getCellMatrix();
		puzzle1.load(save);
		matrix1=puzzle.getCellMatrix();
		boolean entriesMatch = false;
		if(((ValueCell)matrix[1][5]).getValue()==((ValueCell)matrix1[1][5]).getValue()
				&&((ValueCell)matrix[3][4]).getValue()==((ValueCell)matrix[3][4]).getValue()
				&&((ValueCell)matrix[4][3]).getValue()==((ValueCell)matrix[4][3]).getValue()
				&&((ValueCell)matrix[6][5]).getValue()==((ValueCell)matrix[6][5]).getValue()) {
			entriesMatch = true;
			logger.debug("Entries match between sample and saved puzzles.");
			assertTrue(entriesMatch);
		}
		else {
			logger.debug("Entries do not match between sample and saved puzzles.");
			assertFalse(entriesMatch);
		}
		logger.debug("Testing whether saved data and sample match is complete.");
	}
	
	/**
	 * This is a sample solution to use in the testing
	 * @param matrix
	 */
	private void solution(Cell[][] matrix) {
		((ValueCell)matrix[1][5]).setValue(8);
//		((ValueCell)matrix[1][6]).setValue(9);
//		((ValueCell)matrix[2][4]).setValue(2);
//		((ValueCell)matrix[3][2]).setValue(5);
		((ValueCell)matrix[3][4]).setValue(7);
		((ValueCell)matrix[4][3]).setValue(4);
//		((ValueCell)matrix[4][6]).setValue(6);
//		((ValueCell)matrix[5][5]).setValue(9);
//		((ValueCell)matrix[5][6]).setValue(4);
//		((ValueCell)matrix[6][2]).setValue(6);
		((ValueCell)matrix[6][5]).setValue(4);
	}
}
