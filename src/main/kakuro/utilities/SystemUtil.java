package kakuro.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * A utility class for System related operations.
 */
public final class SystemUtil 
{
	// do not instantiate
	private SystemUtil(){}
	
	/**
	 * Represents different OS types.
	 */
	public static enum OSType {
        WINDOWS, MACOSX, LINUX, OTHER
    };

    /**
	 * Represents different system architecture types.
	 */
    public static enum ArchType {
        I386, AMD64
    };
    
    /**
     * @return the lowercase string representation of {@link System#getProperty(String) System.getProperty("os.name")}
     */
    public static String getSystemOSName()
    {
        return System.getProperty("os.name").toLowerCase();
    }

    /**
     * @return the lowercase string representation of {@link System#getProperty(String) System.getProperty("os.arch")}
     */
    public static String getSystemArchName()
    {
        return System.getProperty("os.arch").toLowerCase();
    }

    /**
     * @return an {@link ArchType} representing the current system architecture
     */
    public static ArchType getArchType()
    {
        String arch = getSystemArchName();

        if (arch.contains("64"))
            return ArchType.AMD64;
        else
            return ArchType.I386;
    }

    /**
     * @return an {@link OSType} representing the current operating system
     */
    public static OSType getOSType()
    {
        String osname = getSystemOSName();

        if (osname.contains("win"))
            return OSType.WINDOWS;
        else if (osname.contains("mac"))
            return OSType.MACOSX;
        else if (osname.contains("linux") || osname.contains("nix"))
            return OSType.LINUX;
        else
            return OSType.WINDOWS;
    }
    
    /**
     * BE VERY CAREFULL WITH THIS!!!
     * 
     * If the parameter is a file it deletes it normally. If the parameter is a directory it recursively deletes the
     * directory. (permanently)
     * 
     * BE VERY CAREFULL WITH THIS!!!
     * 
     * @param file the file or directory to recursively delete (permanently)
     * @throws IOException if there is a problem deleting files or directiories
     * 
     * BE VERY CAREFULL WITH THIS!!!
     */
    public static void recusivePermanentDelete(File file) throws IOException
    {
        if (!file.exists())
            return;

        if (file.isDirectory())
        {
            for (File child : file.listFiles())
            {
            	recusivePermanentDelete(child);
            }
        }

        Files.delete(file.toPath());
    }
}
