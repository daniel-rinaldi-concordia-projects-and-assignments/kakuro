package kakuro.utilities;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * A utility class for Common operations.
 */
public final class CommonUtil
{
	// do not instantiate
	private CommonUtil(){}
	
	// Formats
	public static final SimpleDateFormat DATEFORMAT_YMD_HMSs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	/**
	 * cleans a file name string replacing illegal and unwanted characters with underscores.
	 * This function uses the following regex for replacement: [^a-zA-Z0-9_\.\-] <br>
	 * See: {@link String#replaceAll(String, String)}
	 * @param filenameStr the file name string 
	 * @return the cleaned filename
	 */
	public static String cleanFileName(String filenameStr)
    {
    	return filenameStr.replaceAll("[^a-zA-Z0-9_\\.\\-]", "_");
    }
	
	/**
     * Builds a list from the given Entry Set and sorts it lexicographically by calling compareTo(...) on its values.
     * @param <V> a generic type that inherits from Comparable 
     * @param entrySet the set of entries to sort
     * @return a list of sorted entries.
     */
    public static <V extends Comparable<V>> List<Entry<Object, V>> sortEntriesByValues(Set<Entry<Object, V>> entrySet)
    {
        List<Entry<Object, V>> list = new LinkedList<Entry<Object, V>>(entrySet);

        Collections.sort(list, new Comparator<Entry<Object, V>>()
        {
        	@Override
            public int compare(Entry<Object, V> o1, Entry<Object, V> o2)
            {
                return o1.getValue().compareTo((o2).getValue());
            }
        });

        return list;
    }
}
