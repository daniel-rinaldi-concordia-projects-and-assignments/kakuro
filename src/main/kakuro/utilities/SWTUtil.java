package kakuro.utilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * A utility class for SWT related operations.
 */
public final class SWTUtil 
{
	// do not instantiate
	private SWTUtil(){}
	
	/**
	 * Creates a {@link MessageBox} dialog that simply asks the user a yes or no question. 
	 * @param shell the parent widget of the newly created dialog
	 * @param msg the question to ask
	 * @return true if the user answered {@link SWT#OK}, false if the user answered {@link SWT#CANCEL}
	 */
	public static boolean confirmDialogue(Shell shell, String msg)
    {
    	MessageBox dialog = new MessageBox(shell, SWT.ICON_QUESTION | SWT.OK | SWT.CANCEL);
    	dialog.setText("Confirm");
    	dialog.setMessage(msg);
    	return dialog.open() == SWT.OK;
    }
	
	/**
	 * centers the given shell in a parent shell
	 * @param parent the parent shell
	 * @param child the child shell to center
	 */
	public static void centerShellInParent(Shell parent, Shell child)
    {
    	Rectangle parentBounds;
    	if(parent == null)
    		parentBounds = child.getDisplay().getPrimaryMonitor().getBounds();
    	else
    		parentBounds = parent.getBounds();
        
        Rectangle childBounds = child.getBounds();
        
        int x = parentBounds.x + (parentBounds.width - childBounds.width) / 2;
        int y = parentBounds.y + (parentBounds.height - childBounds.height) / 2;
        child.setLocation(x, y);
    }
}
