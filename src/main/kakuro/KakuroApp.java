package kakuro;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import kakuro.controller.AppController;
import kakuro.controller.PuzzleController;
import kakuro.model.Puzzle;
import kakuro.model.SaveObject;
import kakuro.model.data.PuzzleDAO;
import kakuro.model.data.SaveFileDAO;
import kakuro.resources.ResourceUtil;
import kakuro.utilities.CommonUtil;
import kakuro.view.AppView;
import kakuro.view.PuzzleView;
import kakuro.view.WelcomeView;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * The main class that starts the Kakuro application.
 */
public class KakuroApp 
{
	private static Logger logger = LoggerFactory.getLogger(KakuroApp.class);
	
	// Application properties
	public static final float APPLICATION_VERSION = 1.0f;
	public static final String APPLICATION_VERSION_PREFIX = "v_";
	public static final String APPLICATION_NAME = "Kakuro";
	
	private Display display;
	
	private AppView appView;
	
	/**
	 * Constructs a new instance of KakuroApp 
	 */
	public KakuroApp() {
		init();
	}
	
	/**
	 * Initializes game and starts the GUI loop 
	 */
	
	private void init() 
	{
		logger.info("Initializing...");
		
		Configuration.getInstance();
		Configuration.getInstance().setLogLevel(Level.DEBUG);
		
        try // try to enable file logging
        {
            Configuration.getInstance().enableFileLogging(new File(getApplicationVersionDir(), Configuration.ROOT_LOG_FILE_NAME));
        }
        catch (Throwable t)
        {
            logger.warn("Warning, failed to enable file logging:" + t.getMessage());
        }
		
		setupDisplay();
		
		HouseKeeper.doHouseKeeping();
		
		ThemeManager.instance().loadCustomFonts();
		
		// Initialize Model components
		Puzzle puzzleModel = new PuzzleDAO().get(0);
		SaveObject saveObjectModel = new SaveFileDAO().get(0);
		
		// Initialize Controller components
		AppController appController = new AppController(this);
		PuzzleController puzzleController = new PuzzleController(this, puzzleModel, saveObjectModel);
		
		// Initialize View components
		appView = new AppView(this, appController);
		new WelcomeView(appView, puzzleController);
		new PuzzleView(appView, puzzleController, puzzleModel);
	}

	/**
	 * Starts the application and the GUI loop
	 */
	public void start() 
	{
		logger.info("APPLICATION START...");
		logger.info("Application Version: " + APPLICATION_VERSION);
		
		appView.open();
		
		guiLoop();
	}
	
	/**
	 * Basic GUI loop for SWT applications. Calls {@link #shutdown()} when loop ends. 
	 */
	private void guiLoop() 
	{
		while(!appView.getRootComposite().isDisposed())
        {
            if(!display.readAndDispatch())
            	display.sleep();
        }
		shutdown();
	}
	
	/**
	 * Called when the application is shutting down. 
	 * Disposes of system resources associated with this application.
	 * @see Display#dispose()
	 */
	private void shutdown()
	{
		logger.info("APPLICATION END.");
		ResourceUtil.dispose();
		ThemeManager.instance().dispose();
		display.dispose();
	}
	
	/**
	 * @return the application's directory, located in the user's home directory under a sub directory named after the application.
	 * @see #APPLICATION_NAME
	 */
	public static String getApplicationHomeDir()
	{
		return System.getProperty("user.home") + "/" + CommonUtil.cleanFileName(APPLICATION_NAME) + "/";
	}
	
	/**
	 * @return the application's specific version directory, located in the application's home directory under a sub directory named after the version number.
	 * @see #getApplicationHomeDir()
	 * @see #APPLICATION_VERSION_PREFIX
	 * @see #APPLICATION_VERSION
	 */
	public static String getApplicationVersionDir()
	{
		return getApplicationHomeDir() + APPLICATION_VERSION_PREFIX + APPLICATION_VERSION + "/";
	}
	
	/**
	 * Initialize and setup the display
	 */
	private void setupDisplay()
	{
		display = Display.getDefault();
	    display.addFilter(SWT.KeyDown, new Listener() 
	    {
	    	@Override
	    	public void handleEvent(final Event ev)
	    	{
	    		// handle global KeyDown events here...
	    	}
	    });
	}
}

