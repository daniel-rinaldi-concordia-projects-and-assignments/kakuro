package kakuro;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import kakuro.resources.ResourceUtil;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class manages resources and operations related to the look (theme) of the application.
 * This includes things like colors, fonts, etc...
 * This class is a Singleton and follows the singleton design pattern. <br>
 * <br>
 * !!! IMPORTANT !!! Application code must explicitly invoke the {@link #dispose()} method to release the
 * operating system resources managed by cached objects when those objects and OS resources are no longer
 * needed (e.g. on application shutdown)
 */
public class ThemeManager 
{
	// colors
	public static final Color COLOR_DARK = ResourceUtil.getColor(33, 33, 33);
	public static final Color COLOR_LIGHT = ResourceUtil.getColor(250, 250, 250);
	public static final Color COLOR_BLUE = ResourceUtil.getColor(33, 150, 243);
	public static final Color COLOR_RED = ResourceUtil.getColor(244, 67, 54);
	public static final Color COLOR_YELLOW = ResourceUtil.getColor(255, 235, 59);
	public static final Color COLOR_GREEN = ResourceUtil.getColor(76, 175, 80);
	
	// font names
	public static final String FONT_NAME_ROBOTO = "Roboto";
	public static final String FONT_NAME_ROBOTO_LIGHT = "Roboto Light";
	
	// base font properties
	public static final int BASE_FONT_SIZE = 16;
	public static final int BASE_FONT_STYLE = SWT.NORMAL;
	
	// font cache
	private Map<String, Font> fontMap;
	
	// theme components
	public static final Font FONT_TEXT = ResourceUtil.getFont(FONT_NAME_ROBOTO, BASE_FONT_SIZE, BASE_FONT_STYLE);
	public static final Font FONT_BUTTON = ResourceUtil.getFont(FONT_NAME_ROBOTO_LIGHT, BASE_FONT_SIZE, BASE_FONT_STYLE);
	
	// singleton pattern
	private static ThemeManager instance;
	
	// singleton pattern
	private ThemeManager() 
	{ 
		fontMap = new HashMap<String, Font>();
	}
	
	// singleton pattern
	public static ThemeManager instance() 
    { 
        if (instance == null) 
        	instance = new ThemeManager();
  
        return instance; 
    }
	
	/**
	 * Loads all custom fonts needed by the {@link ThemeManager} via {@link Display#loadFont(String)}.
	 */
	public void loadCustomFonts()
	{
		String fontKeyWithoutName = "|"+BASE_FONT_SIZE+"|"+BASE_FONT_STYLE;
		
		if (Display.getCurrent().loadFont(KakuroApp.getApplicationVersionDir() + ResourceUtil.ASSET_FONT_ROBOTO))
			fontMap.putIfAbsent(FONT_NAME_ROBOTO+fontKeyWithoutName, new Font(Display.getCurrent(), FONT_NAME_ROBOTO, BASE_FONT_SIZE, BASE_FONT_STYLE));
		
		if (Display.getCurrent().loadFont(KakuroApp.getApplicationVersionDir() + ResourceUtil.ASSET_FONT_ROBOTO_LIGHT))
			fontMap.putIfAbsent(FONT_NAME_ROBOTO_LIGHT+fontKeyWithoutName, new Font(Display.getCurrent(), FONT_NAME_ROBOTO_LIGHT, BASE_FONT_SIZE, BASE_FONT_STYLE));
	}
	
	/**
	 * Get a {@link Font} based on its name, height and style. 
	 * If the font is not found amongst the fonts made available to the application, then the {@link Display#getSystemFont() default system font} is used instead.
	 * @param fontName the name of the font
	 * @param fontSize the height of the font
	 * @param fontStyle the style of the font ({@link SWT SWT constants})
	 * @return The {@link Font} matching the name, height and style.
	 */
	public Font getFont(String fontName, int fontSize, int fontStyle)
	{
		String fontKey = fontName+"|"+fontSize+"|"+fontStyle;
		if (fontMap.containsKey(fontKey))
			return fontMap.get(fontKey);
		else
		{
			for (FontData fd : Display.getCurrent().getFontList(null, true))
			{
				if (fd.getName().equals(fontName))
				{
					FontData data = new FontData(fd.getName(), fontSize, fontStyle);
					fontMap.put(fontKey, new Font(Display.getCurrent(), data));
					return fontMap.get(fontKey);
				}
			}
		}
		
		FontData data = Display.getCurrent().getSystemFont().getFontData()[0];
		data.setHeight(fontSize);
		data.setStyle(fontStyle);
		fontMap.put(fontKey, new Font(Display.getCurrent(), data));
		return fontMap.get(fontKey);
	}
	
	/**
	 * Dispose of cached objects and their underlying OS resources. This should only be called when the cached
	 * objects are no longer needed (e.g. on application shutdown).
	 */
	public void dispose() 
    {
    	// dispose Fonts
    	for (Font font : fontMap.values())
    		font.dispose();
    }
}
