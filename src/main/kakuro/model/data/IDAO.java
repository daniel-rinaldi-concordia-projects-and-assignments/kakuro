package kakuro.model.data;

import java.util.List;

/**
 * @author Daniel Rinaldi
 * Modified by: Stephanie Mori
 * 
 * This interface specifies functionality of a DAO Design Pattern.
 * @param <T> the type of the data object
 */
public interface IDAO<T> 
{
    public T get(long id);
     
    public List<T> getAll();
     
    public boolean save(T object);
     
    public void update(T object);
     
    public void delete(T object);
}
