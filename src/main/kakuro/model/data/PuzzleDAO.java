package kakuro.model.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import kakuro.exceptions.InvalidDataFormatException;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.SumCell;
import kakuro.model.ValueCell;
import kakuro.resources.ResourceUtil;

/**
 * @author Daniel Rinaldi
 * Modified by: Jason, Stephanie Mori
 * 
 * A concrete {@link IDAO DAO} that encapsulates CRUD operations for a {@link Puzzle}
 */
public class PuzzleDAO implements IDAO<Puzzle>
{
	public static final String PUZZLE_FILE_PREFIX = "puzzle_";
	public static final String PUZZLE_FILE_EXTENSION = ".dat";
			
	
	/**
	 * Gets a {@link Puzzle} by id.
	 * @param id the id of the puzzle resource
	 * @return a {@link Puzzle} instance or null if there was a problem loading the data.
	 */
	@Override
	public Puzzle get(long id) 
	{
		Puzzle puzzle = null;
		
		InputStream istream = null;
		try
		{
			istream = ResourceUtil.getResourceAsStream(ResourceUtil.class, ResourceUtil.DATA_PUZZLES_DIR + PUZZLE_FILE_PREFIX + String.valueOf(id) + PUZZLE_FILE_EXTENSION);
			BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
			
			String line = reader.readLine();
			int nonEmptyLineIndex = 0;
			int puzzleNumRows = 0;
			int puzzleNumColumns = 0;
			while (line != null) 
			{
				line = line.trim();
				if (line.length() == 0) continue;
				
				if (nonEmptyLineIndex == 0) // read first line (should contain metadata about the puzzle)
				{
					String[] tokens = line.split(",");
					
					if (tokens.length < 2) throw new InvalidDataFormatException("Format of puzzle file with id = " + id + " is invalid.");
						
					puzzleNumRows = Integer.parseInt(tokens[0]);
					puzzleNumColumns = Integer.parseInt(tokens[1]);
					
					puzzle = new Puzzle(puzzleNumRows, puzzleNumColumns);
				}
				else if (nonEmptyLineIndex <= puzzleNumRows) // read a row of puzzle cells
				{
					String[] tokens = line.split("\\|");
					
					for (int j = 0; j < puzzleNumColumns; j++) // read each cell in this row
					{
						Cell cell = new Cell();
						
						if (j < tokens.length)
						{
							if (tokens[j].trim().equals("_"))
							{
								cell = new ValueCell(nonEmptyLineIndex-1, j);
							}
						else if (tokens[j].trim().length() >1 && Character.isDigit(tokens[j].trim().charAt(0)))
							{
								String[] sumTokens = tokens[j].split("\\\\");
								
								Integer downSum = null;
								Integer rightSum = null;
								if (sumTokens.length > 0)
								{
									downSum = sumTokens[0].equals("0") ? null : Integer.parseInt(sumTokens[0]);
									if (sumTokens.length > 1)
										rightSum = sumTokens[1].equals("0") ? null : Integer.parseInt(sumTokens[1]);
									else
										rightSum = null;
								}
								
								if (downSum != null || rightSum != null)
									cell = new SumCell(downSum, rightSum);
							}
						}
						
						puzzle.getCellMatrix()[nonEmptyLineIndex-1][j] = cell;
					}
				}
				
				nonEmptyLineIndex++;
				
				line = reader.readLine();
			}
			
			reader.close();
		}
		catch (FileNotFoundException e)
		{
			System.err.println(PuzzleDAO.class.getCanonicalName() + " -- Error: " + e.getMessage());
		} 
		catch (IOException e) 
		{
			System.err.println(PuzzleDAO.class.getCanonicalName() + " -- Error: " + e.getMessage());
		} 
		catch (InvalidDataFormatException e) 
		{
			System.err.println(PuzzleDAO.class.getCanonicalName() + " -- Error: " + e.getMessage());
		}
		
		return puzzle;
	}

	@Override
	public List<Puzzle> getAll() 
	{
		// TODO
		return null;
	}

	@Override
	public boolean save(Puzzle object) 
	{
		return false;
	}

	@Override
	public void update(Puzzle object) 
	{
	}
			

	@Override
	public void delete(Puzzle object) 
	{
		// 
	}
}
