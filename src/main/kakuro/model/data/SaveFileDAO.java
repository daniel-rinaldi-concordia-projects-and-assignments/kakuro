package kakuro.model.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kakuro.KakuroApp;
import kakuro.model.SaveObject;

/**
 * @author Stephanie Mori
 * Modified by: Jason
 * 
 *
 */
/**
 * @author stephaniemori
 *
 */
public class SaveFileDAO implements IDAO<SaveObject>
{
	public static final String SAVE_FOLDER = "saves/";
	public static final String SAVE_FILE_PREFIX = "savedPuzzle_";
	public static final String SAVE_FILE_EXTENSION = ".dat";
	private static Logger logger = LoggerFactory.getLogger(SaveFileDAO.class);
	
	//getter for the class
	@Override
	public SaveObject get(long id) {
		SaveObject saveObject= new SaveObject(id);
		InputStream istream = null;
		
		try 
		{
			istream = new FileInputStream(KakuroApp.getApplicationHomeDir()+SAVE_FOLDER + SAVE_FILE_PREFIX + id + SAVE_FILE_EXTENSION);
			BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
				String line = reader.readLine();
				saveObject.setValueCellData(line.split(","));
				
				reader.close();

		}
		catch(FileNotFoundException e) 
		{
			System.err.println(SaveFileDAO.class.getCanonicalName() +"--- Error: " + e.getMessage());
		}
		catch (IOException e) 
		{
			System.err.println(SaveFileDAO.class.getCanonicalName() + " -- Error: " + e.getMessage());
		}
		
		return saveObject;
	}
	
    //getter for the class
	@Override
    public List<SaveObject> getAll(){
		return null;
    	
    }
    
	/**
	 * Takes a SaveObject that will be saved into the file for future use
	 * 
	 * @param object representing a SaveObject
	 * @return returns true if it was successful, and false if there was a problem
	 */
	@Override
    public boolean save(SaveObject object) {
		int puzzleIndicator = 0;
		String[] puzzleValues = object.getValueCellData();
		try {
			PrintWriter writingStream = new PrintWriter(new FileOutputStream(KakuroApp.getApplicationHomeDir()+ SAVE_FOLDER+
					SAVE_FILE_PREFIX + puzzleIndicator + PuzzleDAO.PUZZLE_FILE_EXTENSION));
			for(int i=0; i<puzzleValues.length;i++) 
			{
				if(i != puzzleValues.length-1)
				{
					writingStream.print(puzzleValues[i]);
					writingStream.print(",");
				}
				else 
				{
					writingStream.print(puzzleValues[i]);
				}
			}
			writingStream.flush();
			writingStream.close();
			return true;
		}
		catch (FileNotFoundException e) {
			logger.error(SaveFileDAO.class.getCanonicalName() + " -- Error: " + "file not found!!" +e.getMessage());
			return false;
		}
		catch (SecurityException e) {
			logger.error(SaveFileDAO.class.getCanonicalName() + " -- Error: " + "security!!! "+ e.getMessage());
			return false;
		}
		catch (Exception e) {
			logger.error(SaveFileDAO.class.getCanonicalName() + " -- Error: " + "other!!! ");
			e.printStackTrace();
			return false;
		}


    }
    
	@Override
    public void update(SaveObject object) {
		
    	
    }
     
	@Override
    public void delete(SaveObject object) {
    	
    }


}
