package kakuro.model;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class represents a sum cell in a Kakuro puzzle grid.
 * A sum cell is a cell that contains 2 sums for use in standard Kakuro gameplay.
 */
public class SumCell extends Cell
{
	private Integer rightSum;
	private Integer downSum;
	
	/**
	 * Constructs an instance of SumCell
	 * @param downSum the sum for the downwards word starting from this sum cell
	 * @param rightSum the sum for the rightwards word starting from this sum cell
	 */
	public SumCell(Integer downSum, Integer rightSum)
	{
		this.downSum = downSum;
		this.rightSum = rightSum;
	}
	
	/**
	 * @return the down sum associated with this cell
	 */
	public Integer getDownSum() 
	{
		return downSum;
	}
	/**
	 * sets the down sum associated with this cell
	 * @param downSum the value to set for the down sum
	 */
	public void setDownSum(Integer downSum) 
	{
		this.downSum = downSum;
	}
	
	/**
	 * @return the right sum associated with this cell
	 */
	public Integer getRightSum() 
	{
		return rightSum;
	}
	/**
	 * sets the right sum associated with this cell
	 * @param rightSum the value to set for the right sum
	 */
	public void setRightSum(Integer rightSum) 
	{
		this.rightSum = rightSum;
	}
}
