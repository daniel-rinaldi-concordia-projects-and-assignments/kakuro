package kakuro.model;

import java.util.ArrayList;
import java.util.List;

import kakuro.IObserver;
import kakuro.ISubject;

/**
 * @author Daniel Rinaldi
 * Modified by: Jason, Stephanie Mori
 * 
 * This class represents a puzzle in a standard Kakuro puzzle game
 */
public class Puzzle implements ISubject
{
	private Cell[][] cellMatrix;
	
	public List<IObserver> observers;
	public enum StateChangeInfo { MATRIX_CHANGE }
	
	/**
	 * Creates a new puzzle matrix instance of size (width x height)
	 * @param width the width in {@link Cell cells} of the kakuro puzzle
	 * @param height the height in {@link Cell cells} of the kakuro puzzle
	 */
	public Puzzle(int width, int height)
	{
		observers = new ArrayList<IObserver>();
		
		this.cellMatrix = new Cell[width][height];
	}
	
	/**
	 * @return the matrix of {@link Cell cells} of this puzzle
	 */
	public Cell[][] getCellMatrix() 
	{
		return cellMatrix;
	}
	
	/**
	 * changes the value of the target cell on the board at position (x, y) to the given character
	 * @param x the column number of the target cell
	 * @param y the row number of the target cell
	 * @param character the character to assign the value loadSavedGameof the target cell to
	 */
	public void editCellValue(int x, int y, char character) 
	{
		if (cellMatrix[x][y] instanceof ValueCell && character >= 49 && character <= 57)
			((ValueCell) cellMatrix[x][y]).setValue(Integer.parseInt(character+""));
	}
	
	/**
	 * deletes the value of the target cell on the board at position (x, y)
	 * @param x the column number of the target cell
	 * @param y the row number of the target cell
	 */
	public void deleteCellValue(int x, int y)
	{
		if (cellMatrix[x][y] instanceof ValueCell)
			((ValueCell) cellMatrix[x][y]).setValue(null);
	}
	
	/**
	 * Checks the current state of the puzzle and determines if it is a solution. 
	 * A puzzle is considered solved when the following 3 conditions are met: <br>
	 * 
	 * 1. Every {@link ValueCell} has a value (not null) <br>
	 * 2. There are no duplicate values in a column or row of {@link ValueCell value cells} 
	 * unless they are separated by a {@link SumCell} or {@link Cell empty cell} <br>
	 * 3. The sum of every row or column of {@link ValueCell value cells} is equal to it's corresponding {@link SumCell}'s value
	 * 
	 * @return true if the puzzle is considered solved, false otherwise.
	 */
	public boolean checkSolution() 
	{
		Cell[][] rows = cellMatrix;
		int sum = 0;
		for (Cell[] row : rows) 
		{
			boolean[] usedDigits = new boolean[10]; //represents the possible digits
			for (Cell cell : row) 
			{
				if (cell instanceof SumCell) 
				{
					//checks if preceding sum is correct
					if (sum != 0) return false;
					
					if (((SumCell) cell).getRightSum() != null)
						sum = ((SumCell) cell).getRightSum();
				}
				else if (cell instanceof ValueCell) 
				{
					Integer value = ((ValueCell) cell).getValue();
					
					if (value == null) //checks if any value cells are blank
						return false;
					else if (usedDigits[value]) //checks if digits are repeated
						return false;
					
					sum -= value;
					usedDigits[value] = true;
				}
			}
		}
		
		//checks if the last sum is indeed correct
		if (sum != 0)
			return false;
		
		//same exact procedure as before, but checking columns
		for (int column = 0; column < rows[1].length; column++) 
		{
			boolean[] usedDigits = new boolean[10];
			for (int row = 0; row < rows.length; row++) 
			{
				Cell cell = rows[row][column];
				if (cell instanceof SumCell) 
				{
					if (sum != 0) return false;
					
					if (((SumCell) cell).getDownSum() != null)
						sum = ((SumCell) cell).getDownSum();
					
					usedDigits = new boolean[10];
				}
				else if (cell instanceof ValueCell) 
				{
					Integer value = ((ValueCell) cell).getValue();
					
					if (usedDigits[value]) return false;
					
					sum -= value;
					usedDigits[value] = true;
				}
			}
		}
		
		if (sum != 0)
			return false;
		
		return true; //returns true only if all rules have been respected
	}
	
	/**Loads saved values of a puzzle into the game board
	 * function then gets the save object as a resource using {@link SaveObject} methods 
	 * then loads values into the puzzle
	 * @param saveData the {@link SaveObject} that contains the save data
	 */
	public void load(SaveObject saveData)
	{
		String[] savedValues = saveData.getValueCellData();
		int savePosition = 0;
		{
			if(savedValues.length >= 0) 
			{
				for(int i=0; i<cellMatrix.length; i++)
				{
					for(int j=0; j<cellMatrix[cellMatrix.length-1].length; j++) 
					{
						if(cellMatrix[i][j] instanceof ValueCell)
						{
							if(Character.isDigit(savedValues[savePosition].charAt(0))) 
							{
								((ValueCell) cellMatrix[i][j]).setValue(Integer.parseInt(savedValues[savePosition]));
								savePosition++;
							}
							else
							{
								savePosition++;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * 
	 */
	public void resetPuzzle() {
		for (Cell[] row : cellMatrix) 
		{
			for (Cell cell : row) 
			{
				if (cell instanceof ValueCell) 
				{ 
					//if valueCell
					Integer value = ((ValueCell) cell).getValue();
					if (value != null) //checks if any value cells have value entered
						((ValueCell)cell).setValue(null);;

				}
			}
		}
	}

	
	@Override
	public void registerObserver(IObserver observer)
	{
		observers.add(observer);
		
		for (int i = 0; i < cellMatrix.length; i++)
		{
			for (Cell cell : cellMatrix[i])
			{
				if (cell instanceof ValueCell)
					((ValueCell) cell).registerObserver(observer);
			}
		}
	}
	
	@Override
	public void unregisterObserver(IObserver observer)
	{
		observers.remove(observer);
		
		for (int i = 0; i < cellMatrix.length; i++)
		{
			for (Cell cell : cellMatrix[i])
			{
				if (cell instanceof ValueCell)
					((ValueCell) cell).unregisterObserver(observer);
			}
		}
	}

	@Override
	public List<IObserver> getObservers() 
	{
		return observers;
	}
}
