package kakuro.model;

import java.util.ArrayList;
import java.util.List;

import kakuro.IObserver;
import kakuro.ISubject;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class represents a value cell in a Kakuro puzzle grid.
 * A value cell is a cell that is blank and accepts input from the user for use in standard Kakuro gameplay.
 */
public class ValueCell extends Cell implements ISubject
{
	private Integer value;
	private int x;
	private int y;
	
	public List<IObserver> observers;
	public enum StateChangeInfo { VALUE_CHANGE }
	
	/**
	 * Constructs an instance of ValueCell
	 * @param x the row that this cell belongs to in a grid
	 * @param y the column that this cell belongs to in a grid
	 */
	public ValueCell(int x, int y)
	{
		this.x = x;
		this.y = y;
		
		observers = new ArrayList<IObserver>();
	}
	
	/**
	 * @return the numerical value stored in this ValueCell
	 */
	public Integer getValue()
	{
		return value;
	}
	/**
	 * sets the numerical value and stores it in this ValueCell
	 * @param value the value to set
	 */
	public void setValue(Integer value)
	{
		this.value = value;
		notifyObservers(StateChangeInfo.VALUE_CHANGE.name());
	}
	
	/**
	 * @return the row that this cell belongs to in a grid
	 */
	public int getX() 
	{
		return x;
	}
	/**
	 * sets the x value of this cell which corresponds to the row that this cell belongs to in a grid
	 * @param x the x value of this cell
	 */
	public void setX(int x)
	{
		this.x = x;
	}
	
	/**
	 * @return the column that this cell belongs to in a grid
	 */
	public int getY()
	{
		return y;
	}
	/**
	 * sets the y value of this cell which corresponds to the column that this cell belongs to in a grid
	 * @param y the y value of this cell
	 */
	public void setY(int y) 
	{
		this.y = y;
	}
	
	@Override
	public List<IObserver> getObservers() 
	{
		return observers;
	}
}
