package kakuro;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * Program driver class
 */
public class Main 
{
	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	/**
	 * Main entry point of the program.
	 * @param args the command-line arguments passed to the program.
	 */
	public static void main(String[] args)
	{
		boolean runApp = true;
		boolean runTests = false;
		
		for (String arg : args)
		{
			if (arg.equalsIgnoreCase("-test")) //use this in terminal to run test in stead of app
			{
				runTests = true;
				runApp = false;
			}
		}

		if (runTests)
		{
			logger.debug("Running Tests...");
			Result result = JUnitCore.runClasses(MainTest.class);
			for (Failure failure : result.getFailures()) {
				logger.debug(failure.toString());
			}
			logger.debug("Were the tests successful?");
			logger.debug(String.valueOf(result.wasSuccessful()));
		}

		if (runApp)
			new KakuroApp().start();
	}
}
