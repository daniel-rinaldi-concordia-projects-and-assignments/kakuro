package kakuro;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.util.FileSize;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class handles all configuration for the app.
 */
public class Configuration 
{
	public static final String ROOT_LOG_FILE_NAME = "kakuro.log";
	
	// singleton
	private static Configuration instance;
	
	private ch.qos.logback.classic.Logger rootLogger;
	
	public static Configuration getInstance()
	{
		if(instance == null)
			instance = new Configuration();
		return instance;
	}
	
	private Configuration()
	{
		initLogging();
	}

	private void initLogging() 
	{
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		rootLogger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
		
		// remove all current appenders
		rootLogger.detachAndStopAllAppenders();
		
		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
	    encoder.setContext(loggerContext);
	    encoder.setPattern("%date{yyyy.MM.dd HH:mm:ss.SSS} [%thread] %c{38} - %level | %msg%n");
	    encoder.start();
	    
	    ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<ILoggingEvent>();
	    consoleAppender.setContext(loggerContext);
	    consoleAppender.setEncoder(encoder);
	    consoleAppender.start();
		
	    rootLogger.addAppender(consoleAppender);
	}
	
	public void setLogLevel(Level level)
	{
		rootLogger.setLevel(level);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    public void enableFileLogging(File logFile)
    {	
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        FileAppender fileAppender = new FileAppender();
        fileAppender.setAppend(false);
        fileAppender.setContext(loggerContext);
        fileAppender.setFile(logFile.getAbsolutePath());

        SizeBasedTriggeringPolicy triggeringPolicy = new SizeBasedTriggeringPolicy();
        triggeringPolicy.setMaxFileSize(FileSize.valueOf("5mb"));
        triggeringPolicy.start();

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        encoder.setPattern("%d %p [%thread] [%c] - %m%n");
        encoder.start();

        fileAppender.setEncoder(encoder);
        fileAppender.start();

        ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.addAppender(fileAppender);
        rootLogger = logbackLogger;
    }
}
