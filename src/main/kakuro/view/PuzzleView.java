package kakuro.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;

import kakuro.ISubject;
import kakuro.ThemeManager;
import kakuro.controller.PuzzleController;
import kakuro.model.Cell;
import kakuro.model.Puzzle;
import kakuro.model.SumCell;
import kakuro.model.ValueCell;
import kakuro.resources.ResourceUtil;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class represents a view of the puzzle in a kakuro game
 * This view is a partial view and belongs to an instance of {@link AppView}
 */
public class PuzzleView extends View
{
	private AppView appView;
	
	// controller dependencies
	private PuzzleController puzzleController;
	
	// GUI components
	private Composite viewComposite;
	private Composite compPuzzleContainer;
	private ScrolledComposite scPuzzleContainer;
	private Composite compPuzzle;
	
	/**
	 * Constructs a new instance of PuzzleView and injects controller and model dependencies
	 * @param appView the {@link AppView} that this view belongs to
	 * @param puzzleController the {@link PuzzleController} that this view is dependent on
	 * @param puzzleModel the {@link Puzzle} that this view is dependent on
	 */
	public PuzzleView(AppView appView, PuzzleController puzzleController, Puzzle puzzleModel)
	{
		super();
		
		this.appView = appView;
		this.puzzleController = puzzleController;
		
		puzzleModel.registerObserver(this);
		
		setupGUI(puzzleModel.getCellMatrix());
	}
	
	@Override
	public void onNotify(ISubject subject, String stateChangeInfo) 
	{
		if (subject instanceof Puzzle)
		{
			Puzzle puzzleModel = (Puzzle) subject;
			if (Puzzle.StateChangeInfo.valueOf(stateChangeInfo) == Puzzle.StateChangeInfo.MATRIX_CHANGE)
			{
				createPuzzle(puzzleModel.getCellMatrix());
			}
		}
		else if (subject instanceof ValueCell)
		{
			ValueCell valueCell = (ValueCell) subject;
			if (ValueCell.StateChangeInfo.valueOf(stateChangeInfo) == ValueCell.StateChangeInfo.VALUE_CHANGE)
			{
				int numColumns = ((GridLayout) compPuzzle.getLayout()).numColumns;
				int childIndex = (valueCell.getX()*numColumns)+valueCell.getY();
				Text txt = ((Text) ((Composite) compPuzzle.getChildren()[childIndex]).getChildren()[0]);
				txt.setData("verifyText", false);
				txt.setText(valueCell.getValue() == null ? "" : String.valueOf(valueCell.getValue()));
				txt.setData("verifyText", true);
			}
		}
	}
	
	@Override
	public Composite getRootComposite() 
	{
		return viewComposite;
	}
	
	/**
	 * Creates a grid representation of a Kakuro puzzle
	 * @param cells the {@link Cell cells} that make up the puzzle
	 */
	private void createPuzzle(Cell[][] cells)
	{
		for (Control control : compPuzzleContainer.getChildren())
			control.dispose();
		
		if (cells.length == 0) return;
		
		compPuzzle = new Composite(compPuzzleContainer, SWT.NONE);
		compPuzzle.setBackground(ThemeManager.COLOR_DARK);
		compPuzzle.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		GridLayout gl_compPuzzle = new GridLayout(cells[0].length, false);
		gl_compPuzzle.horizontalSpacing = 2;
		gl_compPuzzle.verticalSpacing = 2;
		compPuzzle.setLayout(gl_compPuzzle);
		
		for (int i = 0; i < cells.length; i++)
		{
			for (int j = 0; j < cells[i].length; j++)
			{
				Composite compCell = new Composite(compPuzzle, SWT.NONE);
				GridData gd_compCell = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
				gd_compCell.heightHint = 48;
				gd_compCell.widthHint = 48;
				compCell.setLayoutData(gd_compCell);
				
				if (cells[i][j] instanceof ValueCell)
				{
					compCell.setBackground(ThemeManager.COLOR_LIGHT);
					compCell.setCursor(ResourceUtil.getCursor(SWT.CURSOR_HAND));
					
					GridLayout gl_compCell = new GridLayout(1, false);
					gl_compCell.verticalSpacing = 0;
					gl_compCell.marginWidth = 0;
					gl_compCell.marginHeight = 0;
					compCell.setLayout(gl_compCell);
					
					Text txtValueCell = new Text(compCell, SWT.CENTER | SWT.SINGLE);
					txtValueCell.setBackground(ThemeManager.COLOR_LIGHT);
					txtValueCell.setForeground(ResourceUtil.getColor(SWT.COLOR_BLACK));
					txtValueCell.setCursor(ResourceUtil.getCursor(SWT.CURSOR_HAND));
					txtValueCell.setTextLimit(1);
					txtValueCell.setFont(ResourceUtil.getBoldFont(ThemeManager.FONT_TEXT));
					GridData gd_txtValueCell = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
					gd_txtValueCell.widthHint = 16;
					txtValueCell.setLayoutData(gd_txtValueCell);
					int x = i;
					int y = j;
					txtValueCell.setData("verifyText", true);
					txtValueCell.addVerifyListener(new VerifyListener()
					{
						@Override
						public void verifyText(VerifyEvent e) 
						{
							if ((boolean) txtValueCell.getData("verifyText"))
							{
								e.doit = false;
								
								if (e.keyCode == SWT.BS || e.keyCode == SWT.DEL)
									puzzleController.deleteCellValue(x, y);
								else if (e.text.length() > 0)
									puzzleController.editCellValue(x, y, e.text.charAt(0));
							}
						}
					});
					
					compCell.addMouseListener(new MouseAdapter() 
					{
						@Override
						public void mouseDown(MouseEvent e) 
						{
							txtValueCell.setFocus();
						}
					});
				}
				else if (cells[i][j] instanceof SumCell)
				{
					compCell.setBackground(ThemeManager.COLOR_DARK);
					
					SumCell sumCell = (SumCell) cells[i][j];
					compCell.addPaintListener(new PaintListener() 
					{
						@Override
						public void paintControl(PaintEvent e) 
						{
							Rectangle clientRect = compCell.getClientArea();
							e.gc.setForeground(ThemeManager.COLOR_LIGHT);
							e.gc.setFont(ResourceUtil.getFont(e.display.getSystemFont().getFontData()[0].getName(), 14, SWT.NONE));
							
							String strDownSum = sumCell.getDownSum() == null ? "" : String.valueOf(sumCell.getDownSum());
							e.gc.drawString(strDownSum, 0, clientRect.height-e.gc.textExtent(strDownSum).y);
							
							String strRightSum = sumCell.getRightSum() == null ? "" :String.valueOf(sumCell.getRightSum());
							e.gc.drawString(strRightSum, clientRect.width-e.gc.textExtent(strRightSum).x, 0);
							
							e.gc.drawLine(0, 0, clientRect.width, clientRect.height);
						}
					});
				}
				else
				{
					compCell.setBackground(ThemeManager.COLOR_DARK);
				}
			}
		}
		
		scPuzzleContainer.setMinSize(compPuzzleContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	/**
	 * Initialize and setup the GUI
	 * @param puzzleCells the cells of the puzzle to show in this View11
	 */
	private void setupGUI(Cell[][] puzzleCells) 
	{
		viewComposite = new Composite(appView.getStackComposite(), SWT.NONE);
		viewComposite.setData("ViewClass", this.getClass());
		FillLayout fl_viewComposite = new FillLayout(SWT.VERTICAL);
		fl_viewComposite.marginHeight = 5;
		fl_viewComposite.marginWidth = 15;
		viewComposite.setLayout(fl_viewComposite);
		viewComposite.setBackground(ThemeManager.COLOR_DARK);
		
		Composite compContainer = new Composite(viewComposite, SWT.NONE);
		compContainer.setLayout(new GridLayout(1, false));
		compContainer.setBackground(ThemeManager.COLOR_DARK);
	    
		Composite compNav = new Composite(compContainer, SWT.NONE);
		compNav.setBackground(ThemeManager.COLOR_DARK);
		compNav.setLayout(new RowLayout(SWT.HORIZONTAL));
		compNav.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		Button btnMainMenu = new Button(compNav, SWT.NONE);
		btnMainMenu.setText("Main Menu");
		btnMainMenu.setFont(ThemeManager.FONT_BUTTON);
		btnMainMenu.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseDown(MouseEvent e)
			{
				appView.setStackView(WelcomeView.class);
			}
		});
		
		
		Button btnLoad = new Button(compNav,SWT.NONE);
		btnLoad.setFont(ThemeManager.FONT_BUTTON);
		//GridData gd_btnLoad = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		//gd_btnLoad.widthHint = 180;
		//btnLoad.setLayoutData(gd_btnLoad);
		btnLoad.setText("Load");
		btnLoad.addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				puzzleController.loadSavedGame();
				
			}
		});
		
		Button btnReset = new Button(compNav, SWT.NONE);
		btnReset.setText("Reset");
		btnReset.setFont(ThemeManager.FONT_BUTTON);
		btnReset.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseDown(MouseEvent e)
			{
				puzzleController.resetBoard();
			}
		});

		
		scPuzzleContainer = new ScrolledComposite(compContainer, SWT.H_SCROLL | SWT.V_SCROLL);
		scPuzzleContainer.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		scPuzzleContainer.setExpandHorizontal(true);
		scPuzzleContainer.setExpandVertical(true);
		
	    compPuzzleContainer = new Composite(scPuzzleContainer, SWT.BORDER);
		compPuzzleContainer.setBackground(ThemeManager.COLOR_DARK);
		GridLayout gl_compPuzzleContainer = new GridLayout(1, false);
		gl_compPuzzleContainer.verticalSpacing = 0;
		gl_compPuzzleContainer.horizontalSpacing = 0;
		gl_compPuzzleContainer.marginWidth = 0;
		gl_compPuzzleContainer.marginHeight = 0;
		compPuzzleContainer.setLayout(gl_compPuzzleContainer);
	    
		createPuzzle(puzzleCells);
		
		scPuzzleContainer.setContent(compPuzzleContainer);
		
	    Composite compActionsContainer = new Composite(compContainer, SWT.NONE);
	    compActionsContainer.setBackground(ThemeManager.COLOR_DARK);
	    compActionsContainer.setLayout(new GridLayout(1, false));
	    compActionsContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    
	    Composite compButtons = new Composite(compActionsContainer, SWT.NONE);
	    compButtons.setBackground(ThemeManager.COLOR_DARK);
	    compButtons.setLayout(new RowLayout(SWT.HORIZONTAL));
	    compButtons.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
	    
	    Button btnCheckSolution = new Button(compButtons, SWT.NONE);
	    btnCheckSolution.setLayoutData(new RowData(SWT.DEFAULT, 50));
	    btnCheckSolution.setText("Check Solution");
	    btnCheckSolution.setFont(ThemeManager.FONT_BUTTON);
	    btnCheckSolution.addMouseListener(new MouseAdapter() 
	    {
	    	@Override
	    	public void mouseDown(MouseEvent e)
	    	{
	    		if (puzzleController.checkSolution()) {
	    			MessageBox box = new MessageBox((Shell) appView.getRootComposite(), SWT.ICON_INFORMATION | SWT.OK);
	    			box.setText("Congratulations!");
	    			box.setMessage("Puzzle Completed!");
	    			box.open();
	    			appView.setStackView(WelcomeView.class);
	    		}
	    		else {
	    			MessageBox box = new MessageBox((Shell) appView.getRootComposite(), SWT.ICON_INFORMATION | SWT.OK);
	    			box.setText("Incorrect Solution");
	    			box.setMessage("Don't get discouraged! \nCheck if some cells are empty, some numbers within the same column or row are repeated or some sums don't add up.");
	    			box.open();
	    		}
	    	}
		});
	    
	    Button saveProgress = new Button(compButtons, SWT.NONE);
	    saveProgress.setLayoutData(new RowData(SWT.DEFAULT, 50));
	    saveProgress.setText("Save My Progress");
	    saveProgress.setFont(ThemeManager.FONT_BUTTON);
	    saveProgress.addMouseListener(new MouseAdapter() 
	    {
	    	@Override
	    	public void mouseDown(MouseEvent e)
	    	{
	    		if (puzzleController.saveProgress()) {
	    			MessageBox box = new MessageBox((Shell) appView.getRootComposite(), SWT.ICON_INFORMATION | SWT.OK);
	    			box.setText("Saved");
	    			box.setMessage("Your progress has been saved. \nYou may now close the application.");
	    			box.open();
	    			appView.setStackView(WelcomeView.class);
	    		}
	    		else {
	    			MessageBox box = new MessageBox((Shell) appView.getRootComposite(), SWT.ICON_ERROR | SWT.OK);
	    			box.setText("Did Not Save");
	    			box.setMessage("Something went wrong. \nYour puzzle has not been saved. \nPlease try again.");
	    			box.open();
	    		}
	    	}
		});
	}
}
