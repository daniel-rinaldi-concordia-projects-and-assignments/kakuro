package kakuro.view;

import org.eclipse.swt.widgets.Composite;

import kakuro.IObserver;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * An representation of a view in the Kakuro app.
 */
public abstract class View implements IObserver
{
	/**
	 * Constructs a new View instance
	 */
	public View()
	{
		//
	}
	
	/**
	 * @return the root {@link Composite} associated with this view
	 */
	public abstract Composite getRootComposite();
}
