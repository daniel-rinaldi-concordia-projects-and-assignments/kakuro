package kakuro.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;

import kakuro.ISubject;
import kakuro.ThemeManager;
import kakuro.controller.PuzzleController;
import kakuro.resources.ResourceUtil;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class represents a view of a landing page for the user to see before a puzzle or configuration is setup. 
 * This view is a partial view and belongs to an instance of {@link AppView}
 */
public class WelcomeView extends View
{
	private AppView appView;
	
	// GUI components
	private Composite viewComposite;
	
	// controller dependencies
	private PuzzleController puzzleController;
	
	/**
	 * Constructs a new instance of WelcomeView
	 * @param appView the {@link AppView} that this view belongs to
	 * @param puzzleController the {@link PuzzleController} that this view is dependent on
	 */
	public WelcomeView(AppView appView, PuzzleController puzzleController) 
	{
		super();
		
		this.appView = appView;
		this.puzzleController = puzzleController;
		
		setupGUI();
	}
	
	@Override
	public void onNotify(ISubject subject, String state) 
	{
		//
	}
	
	@Override
	public Composite getRootComposite() 
	{
		return viewComposite;
	}
	
	/**
	 * Initialize and setup the GUI
	 */
	private void setupGUI() 
	{
		viewComposite = new Composite(appView.getStackComposite(), SWT.NONE);
		viewComposite.setData("ViewClass", this.getClass());
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		viewComposite.setLayout(fillLayout);
		Image viewCompositeBGImage = ResourceUtil.getImage(ResourceUtil.class, ResourceUtil.IMG_WELCOME_BG);
		viewComposite.addPaintListener(new PaintListener()
		{
			@Override
			public void paintControl(PaintEvent e) 
			{
				Rectangle rect = viewCompositeBGImage.getBounds();
				Rectangle dest = viewComposite.getClientArea();
				e.gc.drawImage(viewCompositeBGImage, rect.x, rect.y, rect.width, rect.height, dest.x, dest.y, dest.width, dest.height);
			}
		});
		
		Composite compContainer = new Composite(viewComposite, SWT.TRANSPARENT);
		compContainer.setBackground(ResourceUtil.getColor(SWT.COLOR_TRANSPARENT));
		GridLayout gl_compContainer = new GridLayout(1, false);
		compContainer.setLayout(gl_compContainer);
		
		Composite compGraphicTitle = new Composite(compContainer, SWT.TRANSPARENT);
		compGraphicTitle.setBackground(ResourceUtil.getColor(SWT.COLOR_TRANSPARENT));
		GridData gd_compGraphicTitle = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compGraphicTitle.heightHint = 274;
		gd_compGraphicTitle.widthHint = 464;
		compGraphicTitle.setLayoutData(gd_compGraphicTitle);
		Image kakuroTitleImage = ResourceUtil.getImage(ResourceUtil.class, ResourceUtil.IMG_KAKURO_TITLE);
		compGraphicTitle.addPaintListener(new PaintListener() 
		{
			@Override
	        public void paintControl(PaintEvent e) 
	        {
				Rectangle src = kakuroTitleImage.getBounds();
				Rectangle dest = compGraphicTitle.getBounds();
				e.gc.drawImage(kakuroTitleImage, src.x, src.y, src.width, src.height, dest.x+(dest.width/2)-src.width/2, dest.y+(dest.height/2)-src.height/2, src.width, src.height);
	        }
	    });
		
		Composite compMenu = new Composite(compContainer, SWT.NONE);
		compMenu.setBackground(ThemeManager.COLOR_DARK);
		compMenu.setLayout(new GridLayout(1, false));
		compMenu.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		
		Button btnPlay = new Button(compMenu, SWT.CENTER);
		btnPlay.setFont(ThemeManager.FONT_BUTTON);
		GridData gd_btnPlay = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
		gd_btnPlay.widthHint = 180;
		btnPlay.setLayoutData(gd_btnPlay);
		btnPlay.setText("Play");
		btnPlay.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseDown(MouseEvent e) 
			{
				puzzleController.resetBoard();
				appView.setStackView(PuzzleView.class);
			}
		});
		
		Button btnQuit = new Button(compMenu, SWT.CENTER);
		btnQuit.setFont(ThemeManager.FONT_BUTTON);
		GridData gd_btnQuit = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnQuit.widthHint = 180;
		btnQuit.setLayoutData(gd_btnQuit);
		btnQuit.setText("Quit");
		btnQuit.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseDown(MouseEvent e) 
			{
				viewComposite.getShell().close();
			}
		});
	} }