package kakuro.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import kakuro.ISubject;
import kakuro.KakuroApp;
import kakuro.ThemeManager;
import kakuro.controller.AppController;
import kakuro.resources.ResourceUtil;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class represents a view of a Kakuro game. 
 * This view is displayed as a Shell widget (window). 
 * This view owns a composite which contains a stack of partial view {@link Composite composites} 
 * in which only the top view in that stack is displayed.
 */
public class AppView extends View 
{
	private static final int WINDOW_MIN_WIDTH = 800;
	private static final int WINDOW_MIN_HEIGHT = 600;
	
	// controller dependencies
	@SuppressWarnings("unused") private AppController appController;
	
	// GUI components
	private Shell window;
	private ScrolledComposite scrollArea;
	private Composite compMain;
	
	/**
	 * Constructs a new instance of AppView
	 * @param app the {@link KakuroApp}
	 * @param appController the {@link AppController} that this view depends on
	 */
	public AppView(KakuroApp app, AppController appController)
	{
		super();
		
		this.appController = appController;
		
		setupGUI();
	}

	@Override
	public void onNotify(ISubject subject, String state) 
	{
		//
	}
	
	@Override
	public Composite getRootComposite() 
	{
		return window;
	}
	
	/**
	 * @return the {@link Composite} that contains the stacked partial views
	 */
	public Composite getStackComposite()
	{
		return compMain;
	}
	
	/**
	 * opens the window
	 */
	public void open() 
	{
		window.open();
		window.setMaximized(true);
		
		if (compMain.getChildren().length > 0)
		{
			((StackLayout) compMain.getLayout()).topControl = compMain.getChildren()[0];
			compMain.layout();
			scrollArea.setMinSize(compMain.getChildren()[0].getSize());
		}
	}
	
	/**
	 * sets the partial view that is shown in the composite that contains the stacked partial views
	 * @param compositeViewClazz The class of the associated with the partial view to switch to
	 */
	public void setStackView(Class<? extends View> compositeViewClazz)
	{
		for (Control stackedChild : compMain.getChildren())
		{
			if (stackedChild.getData("ViewClass") == compositeViewClazz)
			{
				((StackLayout) compMain.getLayout()).topControl = stackedChild;
				compMain.layout();
				scrollArea.setMinSize(stackedChild.getSize());
				
			}
		}
	}
	
	/**
	 * Initialize and setup the shell widget (window).
	 */
	private void setupWindow() 
	{
		window = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.MIN | SWT.MAX | SWT.RESIZE);
		window.setBackground(ThemeManager.COLOR_DARK);
		window.setLayout(new FillLayout(SWT.VERTICAL));
		window.setMinimumSize(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT);
		window.setText(KakuroApp.APPLICATION_NAME + " - " + KakuroApp.APPLICATION_VERSION);
		
		Monitor primaryMonitor = window.getDisplay().getPrimaryMonitor();
        Rectangle monitorBounds = primaryMonitor.getBounds();
        Rectangle windowBounds = window.getBounds();
        
        int x = monitorBounds.x + (monitorBounds.width - windowBounds.width) / 2;
        int y = monitorBounds.y + (monitorBounds.height - windowBounds.height) / 2;
        
        window.setLocation(x, y);
        
        window.setActive();
        
        try
        {
        	window.setImage(ResourceUtil.getImage(ResourceUtil.class, ResourceUtil.IMG_FAVICON));
        }
        catch (Exception e)
        {
            System.err.println("Error setting main window icon: " + e.getMessage());
        }
        
        window.addKeyListener(new KeyAdapter()
	    {
	    	@Override
	    	public void keyPressed(final KeyEvent e)
	    	{
	    		// handle window KeyDown events here...
	    	}
		});
        
        window.addListener(SWT.Resize, new Listener() 
        {
			@Override
			public void handleEvent(Event event) 
			{
				if (compMain != null && compMain.getChildren().length > 0)
					scrollArea.setMinSize(compMain.getChildren()[0].computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
        });
        
        window.addListener(SWT.Close, new Listener() 
        {
        	@Override
            public void handleEvent(Event event) 
            {
            	// window is closing
            }
        });
	}
	
	/**
	 * Initialize and setup the window's menu bar.
	 */
	private void setupMenuBar() 
	{
		Menu menuBar = new Menu(window, SWT.BAR);
		
	    MenuItem fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
	    fileMenuHeader.setText("File");

	    Menu fileMenu = new Menu(window, SWT.DROP_DOWN);
	    fileMenuHeader.setMenu(fileMenu);
	    
	    MenuItem fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
	    fileExitItem.setText("Exit");
	    fileExitItem.addSelectionListener(new SelectionAdapter()
	    {
	    	@Override
	    	public void widgetSelected(SelectionEvent event)
	    	{
	    		window.close();
	    	}
	    });
	    
	    MenuItem helpMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
	    helpMenuHeader.setText("Help");

	    Menu helpMenu = new Menu(window, SWT.DROP_DOWN);
	    helpMenuHeader.setMenu(helpMenu);
	    
	    window.setMenuBar(menuBar);
	}
	
	/**
	 * Initialize and setup the GUI
	 */
	private void setupGUI() 
	{
		setupWindow();
		
		setupMenuBar();
		
		Composite compBody = new Composite(window, SWT.NONE);
	    GridLayout gl_compBody = new GridLayout(1, false);
	    gl_compBody.marginWidth = 0;
	    gl_compBody.marginHeight = 0;
	    gl_compBody.verticalSpacing = 0;
	    compBody.setLayout(gl_compBody);
		
		scrollArea = new ScrolledComposite(compBody, SWT.H_SCROLL | SWT.V_SCROLL);
	    scrollArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	    scrollArea.setExpandHorizontal(true);
	    scrollArea.setExpandVertical(true);
	    
	    compMain = new Composite(scrollArea, SWT.NONE);
	    StackLayout sl_compMain = new StackLayout();
	    compMain.setLayout(sl_compMain);
	    
	    scrollArea.setContent(compMain);
	}
}
