package kakuro.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * Custom button class. It's basically a label that's posing as a button.
 * Using a label means that the look and feel will be the same cross platform.
 */
public class CustomButton extends CLabel
{
	private Color bgColor;
	private Color bgColorHover;
	private Color txtColor;
	private Color txtColorHover;
	
	Cursor cursor;
	
	/**
	 * Constructs and instance of CustomButton
	 * @param parent a widget which will be the parent of the new instance (cannot be null)
	 * @param style the style of the widget to construct
	 */
	public CustomButton(Composite parent, int style) 
	{
		super(parent, style);
		
		bgColor = this.getBackground();
		bgColorHover = bgColor;
		txtColor = this.getForeground();
		txtColorHover = txtColor;
		
		setupGUI();
	}

	/**
	 * Sets the background and foreground colors when user mouses over this button.
	 * @param colorBG the background color
	 * @param colorTXT the foreground color
	 */
	public void setHoverColors(Color colorBG, Color colorTXT)
	{
		bgColorHover = colorBG;
		txtColorHover = colorTXT;
	}
	
	/**
	 * Sets the background and foreground colors of this button.
	 * @param colorBG the background color
	 * @param colorTXT the foreground color
	 */
	public void setColors(Color colorBG, Color colorTXT)
	{
		this.setBackground(colorBG);
		this.setForeground(colorTXT);
		bgColor = colorBG;
		txtColor = colorTXT;
	}
	
	@Override
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		
		if(enabled)
			setForeground(txtColor);
		else
			setForeground(bgColor); // temporary style for disabled button (should be something nicer)
	}
	
	/**
	 * Initialize and setup the GUI
	 */
	private void setupGUI() 
	{
		cursor = new Cursor(Display.getDefault(), SWT.CURSOR_HAND);
		this.setCursor(cursor);
		
		this.addMouseTrackListener(new MouseTrackAdapter() 
		{
			@Override
	    	public void mouseEnter(MouseEvent e) 
	    	{
				if(getEnabled())
				{
					setBackground(bgColorHover);
					setForeground(txtColorHover);
				}
	    	}
		});
		
		this.addMouseTrackListener(new MouseTrackAdapter() 
		{
	    	@Override
	    	public void mouseExit(MouseEvent e) 
	    	{
	    		if(getEnabled())
				{
		    		setBackground(bgColor);
	    			setForeground(txtColor);
				}
	    	}
		});
	}
	
	@Override
	public void dispose()
	{
		cursor.dispose();
	}
}
