package kakuro;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This interface specifies functionality of an observer in the Observer Design Pattern.
 * A class can implement this interface when it wants to be informed of changes in subject objects.
 */
public interface IObserver 
{
	/**
	 * This function is called by the subjects in which we've registered ourselves with when their state changes.
	 * @param subject A reference to the Subject who notified us.
	 * @param stateChangeInfo Information about what has changed in the subject's state. 
	 */
	public void onNotify(ISubject subject, String stateChangeInfo);
}
