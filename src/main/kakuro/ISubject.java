package kakuro;

import java.util.List;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This interface specifies functionality of a subject in the Observer Design Pattern.
 */
public interface ISubject 
{
	/**
	 * @return a reference to the list of observers who have been registered to this object via {@link #registerObserver(IObserver)}
	 */
	abstract List<IObserver> getObservers();
	
	/**
	 * Registers the observer with this object so that it can be notified when the state changes.
	 * @param observer the observer to register
	 */
	public default void registerObserver(IObserver observer)
	{
		if (getObservers() != null)
			getObservers().add(observer);
	}
	
	/**
	 * Unregisters the observer with this object so that it will not be notified when the state changes.
	 * @param observer the observer to unregister
	 */
	public default void unregisterObserver(IObserver observer)
	{
		if (getObservers() != null)
			getObservers().remove(observer);
	}
	
	/**
	 * This function should be called when observers should be notified of a change in state.
	 * @param stateChangeInfo A string containing info about what has changed.
	 */
	public default void notifyObservers(String stateChangeInfo)
	{
		if (getObservers() != null)
		{
			for (IObserver observer : getObservers())
				observer.onNotify(this, stateChangeInfo);
		}
	}
}
