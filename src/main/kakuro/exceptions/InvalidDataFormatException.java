package kakuro.exceptions;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * Thrown to indicate that a problem occurred due to some data being improperly formatted. <br>
 * Applications can subclass this class to indicate similar exceptions.
 */
@SuppressWarnings("serial")
public class InvalidDataFormatException extends Exception
{
    public static final String ERROR = "An invalid format has been detected.";
    
    /**
     * Constructs a new {@link InvalidDataFormatException} with no detail message.
     */
    public InvalidDataFormatException() 
    {
        super(InvalidDataFormatException.ERROR);
    }
    
    /**
     * Constructs a new {@link InvalidDataFormatException} with the specified detail message.
     * @param message the detail message.
     */
    public InvalidDataFormatException(String message) 
    {
        super(InvalidDataFormatException.ERROR + "\n" + message);
    }
    
    /**
     * Constructs a new {@link InvalidDataFormatException} with the specified detail message and cause. 
     * @param message the detail message
     * @param cause the cause of the exception. (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public InvalidDataFormatException(String message, Throwable cause) 
    {
        super(InvalidDataFormatException.ERROR + "\n" + message, cause);
    }
}
