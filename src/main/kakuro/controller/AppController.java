package kakuro.controller;

import kakuro.KakuroApp;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * A controller that handles the business logic related to the application
 */
public class AppController extends Controller 
{
	/**
	 * Constructs an instance of this Controller.
	 * @param app a reference to the main application.
	 */
	public AppController(KakuroApp app) 
	{
		super(app);
	}
}
