package kakuro.controller;

import kakuro.KakuroApp;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * Base controller class for the application. 
 * All concrete controller implementations should inherit from this class.
 */
public class Controller 
{
	protected KakuroApp app;
	
	/**
	 * Constructs an instance of this Controller.
	 * @param app a reference to the main application.
	 */
	public Controller(KakuroApp app)
	{
		this.app = app;
	}
}
