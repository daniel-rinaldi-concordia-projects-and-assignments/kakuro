package kakuro.controller;

import kakuro.KakuroApp;
import kakuro.model.*;
import kakuro.model.data.SaveFileDAO;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * A controller that handles the business logic related to Puzzle operations
 */
/**
 * @author TenaciousDan
 *
 */
public class PuzzleController extends Controller
{
	//public static final String SAVE_FILE_PREFIX = "load-test";
	//public static final String SAVE_FILE_EXTENSION = ".dat";

	private Puzzle puzzleModel;
	private SaveObject saveObjectModel;
	
	/**
	 * Constructs an instance of PuzzleController.
	 * @param app a reference to the main application.
	 * @param puzzleModel a puzzle model that this controller can manipulate.
	 * @param saveObjectModel a save object model that this controller can manipulate.
	 */
	public PuzzleController(KakuroApp app, Puzzle puzzleModel, SaveObject saveObjectModel)
	{
		super(app);
		
		this.puzzleModel = puzzleModel;
		this.saveObjectModel = saveObjectModel;
	}
	
	/**
	 * Constructs an instance of PuzzleController, for the unit tests to use
	 * @param app a reference to the main application.
	 * @param puzzleModel a puzzle model that this controller can manipulate.
	 */
	public PuzzleController(KakuroApp app, Puzzle puzzleModel)
	{
		super(app);
		
		this.puzzleModel = puzzleModel;
	}

	/**
	 * changes the value of the target cell at position (x, y) to the given character on the current puzzle
	 * @param x the column number of the target cell
	 * @param y the row number of the target cell
	 * @param character the character to assign the value loadSavedGameof the target cell to
	 */
	public void editCellValue(int x, int y, char character) 
	{
		puzzleModel.editCellValue(x, y, character);
	}
	
	/**
	 * deletes the value of the target cell at position (x, y) on the current puzzle
	 * @param x the column number of the target cell
	 * @param y the row number of the target cell
	 */
	public void deleteCellValue(int x, int y)
	{
		puzzleModel.deleteCellValue(x, y);
	}
	
	
	/**
	 * loads the last saved game.
	 */
	public void loadSavedGame() 
	{
		puzzleModel.load(saveObjectModel);
	}
	
	/**
	 * Check if the solution of the current puzzle is correct.
	 * @return true if the current solution is correct, false otherwise
	 */
	public boolean checkSolution() 
	{
		return puzzleModel.checkSolution();
	}
	
	/**
	 * Saves the current state of the puzzle to an outside file to be able to be reloaded in the future.
	 * This function opens the file, saves the current cells' state into the file and closes the file.
	 * @return true if the file was accessed successfully and false if it was not able to get access
	 */
	public boolean saveProgress() 
	{
		saveObjectModel.setValueCellData(puzzleModel);

		return (new SaveFileDAO().save(saveObjectModel));
	}
	
	/**
	 * Changes any entries on the board back to empty cells - making the board blank again
	 */
	public void resetBoard() 
	{
		puzzleModel.resetPuzzle();
	}

	
}
