package kakuro;

import java.io.File;
import java.io.IOException;

import kakuro.resources.ResourceUtil;
import kakuro.utilities.SystemUtil;

/**
 * @author Daniel Rinaldi
 * Modified by: 
 * 
 * This class handles housekeeping tasks. Housekeeping tasks are tasks that involve manipulating the file system, 
 * specifically in a folder associated with the application (usually located in user's home directory)
 */
public class HouseKeeper 
{
	/**
     * Performs housekeeping tasks.
     */
	public static void doHouseKeeping()
	{
		// start async tasks
		cleanupAsync(null);
		
		// non async tasks
		createHome();
		createSaveDir();
		
		try { installCustomFonts(); }
		catch (IOException e)
		{
			System.err.println(HouseKeeper.class.getCanonicalName() + " -- Error trying to copy internal font to file system: Either the internal or external file was not found or an io exception occurred. " + e.getMessage());
		}
	}
	
	/** 
	 * Creates the application's home directory if it does not exist.
	 */
	private static void createHome() 
	{
		File homeFolder = new File(KakuroApp.getApplicationHomeDir());
        if(!homeFolder.exists())
        {
            try
            {
            	if (!homeFolder.mkdirs())
            		throw new Exception("Some directories were not created '" + homeFolder.toPath() + "'");
            }
            catch (Exception e)
            {
                System.err.println(HouseKeeper.class.getCanonicalName() + " -- Error while trying to create the application's home directory: " + e.getMessage());
            }
        }
	}
	
	/** 
	 * Creates the application's saves directory if it does not exist.
	 */
	private static void createSaveDir() 
	{
		File saveFolder = new File(KakuroApp.getApplicationHomeDir()+"saves/");
        if(!saveFolder.exists())
        {
            try
            {
            	if (!saveFolder.mkdirs())
            		throw new Exception("Some directories were not created '" + saveFolder.toPath() + "'");
            }
            catch (Exception e)
            {
                System.err.println(HouseKeeper.class.getCanonicalName() + " -- Error while trying to create the application's home directory: " + e.getMessage());
            }
        }
	}

	/**
     * Does all the work needed to leave the home directory in a consistent state. This method must also prevent the home folder from getting too big in size (delete unneeded/old files).
     * This method starts and runs in a new Thread and once finished, calls the callback Runnable.
	 * @param callback a Runnable to run once cleanup is complete.
	 */
	private static void cleanupAsync(final Runnable callback) 
	{
		new Thread()
        {
			@Override
            public void run()
            {
                this.setName("HouseKeeping_cleanup");
                
                File homeFolder = new File(KakuroApp.getApplicationHomeDir());
                File[] lsFiles = homeFolder.listFiles();
                
                // loop through files in home directory and cleanup any unneeded/old files.
                if(lsFiles != null)
                {
                	for(File file : lsFiles)
                	{
                		// cleanup old versions
                		if (file.isDirectory() && file.getName().startsWith(KakuroApp.APPLICATION_VERSION_PREFIX) && !file.toPath().equals(new File(KakuroApp.getApplicationVersionDir()).toPath()))
                		{
                			try { SystemUtil.recusivePermanentDelete(file); }
                			catch (IOException e)
                			{
                				System.err.println(HouseKeeper.class.getCanonicalName() + " -- Error while trying to cleanup old version (" + KakuroApp.APPLICATION_VERSION + "): " + e.getMessage());
                			}
                		}
                	}
                }
                
                if (callback != null)
                	callback.run();
            }
        }.start();
	}
	
	
	/**
	 * Installs fonts needed by the application. The fonts can then be loaded by the application via Display.loadFont(...)
	 * @throws IOException If the external font file was not found or could not be copied to the file system
	 */
	private static void installCustomFonts() throws IOException
	{
		String appVersionDir = KakuroApp.getApplicationVersionDir();
		
		if (!new File(appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO_LICENSE).exists())
			ResourceUtil.copyResourceToFileSystem(ResourceUtil.ASSET_FONT_ROBOTO_LICENSE, appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO_LICENSE);
		
		if (!new File(appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO).exists())
			ResourceUtil.copyResourceToFileSystem(ResourceUtil.ASSET_FONT_ROBOTO, appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO);
		
		if (!new File(appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO_LIGHT).exists())
			ResourceUtil.copyResourceToFileSystem(ResourceUtil.ASSET_FONT_ROBOTO_LIGHT, appVersionDir + ResourceUtil.ASSET_FONT_ROBOTO_LIGHT);
	}
}
