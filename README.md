# Kakuro Puzzle Game

project description goes here...

## Getting Started

**Clone the git repo:**  
`cd parent/directory/that/you/will/clone/the/project/to`
`git clone https://gitlab.com/comp-354-pj-b/kakuro.git`

### Eclipse:  
**Import the project into the workspace:**  
- open Eclipse and select the parent directory that you cloned the project to as the workspace directory
- Next, in the package explorer view click `Import Projects...` and select `General` > `Projects from Folder or Archive`
- In the next window, for Import Source field select the project directory and then press Finish.

**Remove any swt binaries, that are not for your platform, from the build path:**  
- example: if you are using windows 64-bit OS then, in Eclipse, under `Referenced Libraries` right click the other jar files that start with `swt-` that are for linux and mac and select `Build Path` > `Remove from Build Path`

**Add the appropriate swt binary to your build bath:** (swt binaries are located in the `Kakuro/lib/` folder)  
- example: if you are using windows 64-bit OS then, in Eclipse, right click the file `Kakuro/lib/swt-4.14-win32-win32-x86_64.jar` and select `Build Path` > `Add to Build Path`

**Add the JUnit jar to your build bath:** (the JUnit jar is located in the `Kakuro/lib/` folder)  
- right click the file `Kakuro/lib/junit-platform-console-standalone-1.6.0` and select `Build Path` > `Add to Build Path`

**Add the logging libraries to your build bath:** (these jars are located in the `Kakuro/lib/` folder)  
- right click the file `Kakuro/lib/logback-classic-1.2.3.jar` and select `Build Path` > `Add to Build Path`
- right click the file `Kakuro/lib/logback-core-1.2.3.jar` and select `Build Path` > `Add to Build Path`
- right click the file `Kakuro/lib/slf4j-api-1.7.9.jar` and select `Build Path` > `Add to Build Path`

**Create a Run Configuration:**  
- In Eclipse, click the dropdown arrow next to the play button in the top bar and select `Run Configurations...`
- Next, in the left hand menu of the Run Configurations window, right click `Java Application` > `New Configuration`
- give the Run Configuration a name, for the Project field browse and select `kakuro`, for the Main class field select `kakuro.Main`
- click `Apply` then `Run`

## Building runnable jar file for distribution  
### Eclipse:  
**Run the ant build script:**  
- In the package explorer view, right click the file `build.xml` located in the root project directory and select `Run As` > `Ant Build`
- If the build was successful (you should be notified in the console), right click on the project and select `Refresh`. There should be a new directory created called `dist`.
- The runnable jar file should be located at `dist/${target.os.arch}/${target.version}/kakuro.jar`
- double click the jar file or run `java -jar` command to launch the app

## Building documentation
- Make sure you have some version of pdflatex installed. 
	- On linux this is provided by the texlive package
	- On windows [Get texlive here](http://www.tug.org/texlive/acquire-netinstall.html)
	- On mac [Get mactex here](https://tug.org/mactex/)
- The documentation can be built by running `pdflatex src/main.tex` twice from the documentation folder, or by using any other latex build system.
